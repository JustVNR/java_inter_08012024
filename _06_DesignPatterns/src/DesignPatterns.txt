Historique :

1994 le Ganag Of Four (GoF) publie un livre intitulé : 
"Design Patterns:Elements of Reusable Object-Oriented Software"

Ce livre recence un ensemble de design patterns classables en 3 catégories :

- Patterns de Comportement
- Patterns de Creation
- Patterns de Structure