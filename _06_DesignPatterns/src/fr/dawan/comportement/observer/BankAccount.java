package fr.dawan.comportement.observer;

import java.util.ArrayList;
import java.util.List;

public class BankAccount implements Subject<Double> {

	private String numero;

	private double solde;

	private List<Observer<Double>> observers;

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
		notifyObserver(solde);
		// QUESTION : METTRE LA LOGIQUE SOLDE > 0 ICI OU PAS ?
		// REPONSE : Ne pas mettre la logique qui vise à tester la valeur du solde ici
		// permet d'utiliser la class BankAcount avec différents types d'observateurs
		// qui ne serait pas tous intéressés par les mêmes types de changement de valeur solde
	}

	public BankAccount(String numero, double solde) {
		super();
		this.numero = numero;
		this.solde = solde;
		this.observers = new ArrayList<Observer<Double>>();
	}

	@Override
	public String toString() {
		return "BankAccount [numero=" + numero + ", solde=" + solde + "]";
	}

	@Override
	public void register(Observer<Double> obs) {

		observers.add(obs);
	}

	@Override
	public void unRegister(Observer<Double> obs) {

		observers.remove(obs);
	}

	@Override
	public void notifyObserver(Double solde) {

		observers.forEach(o -> o.update(solde));
	}
}
