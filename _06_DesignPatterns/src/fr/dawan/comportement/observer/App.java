package fr.dawan.comportement.observer;

public class App {

	/*
	 * Le pattern Observer définit une dépendance entre des sujets d'observation et
	 * des observateurs de telle sorte que ces derniers soient notifiés dès lors
	 * qu'un sujet d'observation change d'état.
	 * 
	 * 
	 */
	public static void main(String[] args) {

		Product prod = new Product("PC", 1200);
		
		Observer<Double> observer1 = new Client("Client 1");
		Observer<Double> observer2 = new Client("Client 2");
		
		prod.register(observer1);
		prod.register(observer2);
		
		prod.setPrix(800); // Ce changment doit déclencher 2 notifs...
		
		System.out.println(observer1);
		
		System.out.println("\n============== BANK ACCOUNT OBSERVER =================\n");
		
		// Sujet Compte bancaire : notifier customer si solde négatif
		
		BankAccount account = new BankAccount("aaaa", 500);
		
		Observer<Double> observer3 = new Customer("Customer 1");
		
		account.register(observer3);
		
		account.setSolde(1000);
		
		account.setSolde(-1);
		
		System.out.println(observer3);
		
		// notification de solde négatif : -1.0€
		// Customer [name=Customer 1, lastObservedPrice = -1.0]

	}
}
