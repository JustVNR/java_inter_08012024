package fr.dawan.comportement.observer;

public class Client implements Observer<Double> {

	private String name;

	private double lastObservedPrice;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLastObservedPrice() {
		return lastObservedPrice;
	}

	public Client(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Client [name=" + name + ", lastObservedPrice=" + lastObservedPrice + "]";
	}

	@Override
	public void update(Double prix) {

		System.out.println(name + " : notification de prix reçue : " + prix);
		
		lastObservedPrice = prix;
	}
}
