package fr.dawan.comportement.observer;

public class Customer implements Observer<Double> {

	private String name;
	
	private double lastObservedSolde;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLastObservedSolde() {
		return lastObservedSolde;
	}
	
	public Customer(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", lastObservedSolde=" + lastObservedSolde + "]";
	}

	@Override
	public void update(Double solde) {

		if(solde < 0) {
			System.out.println("Notification de solde négatif : " + solde + "€");
		}
		lastObservedSolde = solde; // A voir si on met à jour lorsque le solde n'est pas négatif...
	}
}
