package fr.dawan.comportement.observer;

public interface Subject<T> {

	void register(Observer<T> obs);
	
	void unRegister(Observer<T> obs);
	
	void notifyObserver(T obj);
}
