package fr.dawan.comportement.observer;

public interface Observer<T> {

	void update(T p);
}
