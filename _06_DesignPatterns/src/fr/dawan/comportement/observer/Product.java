package fr.dawan.comportement.observer;

import java.util.ArrayList;
import java.util.List;

public class Product implements Subject<Double> {

	private String description;
	
	private double prix;
	
	private List<Observer<Double>> observers;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		
		this.prix = prix;
		
		notifyObserver(prix); // <===== Notification lors d'un changement de prix
	}
	
	public Product(String description, double prix) {
		super();
		this.description = description;
		this.prix = prix;
		this.observers = new ArrayList<Observer<Double>>(); // Initialisation de la liste d'observateurs
	}

	@Override
	public String toString() {
		return "Product [description=" + description + ", prix=" + prix + "]";
	}

	@Override
	public void register(Observer<Double> obs) {

		observers.add(obs);
		notifyObserver(prix);
	}

	@Override
	public void unRegister(Observer<Double> obs) {
		
		observers.remove(obs);
	}

	@Override
	public void notifyObserver(Double obj) {

		observers.forEach(o -> o.update(obj));
	}
}
