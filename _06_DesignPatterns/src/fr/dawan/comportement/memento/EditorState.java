package fr.dawan.comportement.memento;

public class EditorState {

	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public EditorState(String content) {
		super();
		this.content = content;
	}
}
