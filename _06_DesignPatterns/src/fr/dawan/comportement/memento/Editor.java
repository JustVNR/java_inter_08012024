package fr.dawan.comportement.memento;

public class Editor {
	
	private History history;
	
	public String getContent() {
		return history.getCurrentContent();
	}
	
	public void setContent(String content) {
		history.push(new EditorState(content));
	}
	
	public Editor() {
		
		history = new History();
		
		history.push(new EditorState(""));
	}
	
	public Editor(String content) {
		history = new History();
		
		history.push(new EditorState(content));
	}
	
	public void undo() throws Exception {
		history.pop();
	}
}
