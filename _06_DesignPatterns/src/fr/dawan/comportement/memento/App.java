package fr.dawan.comportement.memento;

public class App {

	/*
	 * Le pattern Memento permet de capturer et de restaurer l'état interne d'un
	 * objet sans révéler les détails de son implémentation. Il est souvent utilisé
	 * pour mettre en oeuvre des mécanismes de retour en arrière (undo) ou plus
	 * généralement de restauration d'état dans une application.
	 */

	public static void main(String[] args) {

		Editor editor = new Editor();

		editor.setContent("a");
		editor.setContent("ab");
		editor.setContent("abc");

		System.out.println("Contenu actuel de l'éditeur : " + editor.getContent());

		try {
			editor.undo();

			System.out.println("1er restore : " + editor.getContent()); // ab

			editor.undo();

			System.out.println("2 ième restore : " + editor.getContent()); // a

			editor.undo();

			System.out.println("3 ième restore : " + editor.getContent()); // 
			
			editor.undo();

			System.out.println("4 ième restore : " + editor.getContent()); // Exception
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

}
