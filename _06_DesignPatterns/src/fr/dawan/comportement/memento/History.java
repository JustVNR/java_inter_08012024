package fr.dawan.comportement.memento;

import java.util.ArrayList;
import java.util.List;

public class History {

	private List<EditorState> states;
	
	public History() {
		states = new ArrayList<EditorState>();
	}
	
	public void push (EditorState state) {
		states.add(state);
	}
	
	public void pop() throws Exception {
		
		if(states.size() <= 1) {
			
			throw new Exception("L'historique est vide");
		}
		states.remove(states.get(states.size() - 1));
	}
	
	public String getCurrentContent() {
		
		return states.get(states.size() - 1).getContent();
	}
}
