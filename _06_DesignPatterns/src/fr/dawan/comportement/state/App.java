package fr.dawan.comportement.state;

public class App {

	/*
	 * Le pattern State permet à un objet d'adapter son comportement à son état
	 * interne. L'objet est représenté comme une machine à états finis.
	 * 
	 * Chacun de ces états est encapsulé dans une classe distincte.
	 */
	
	/**
	 * Avantages du pattern "State":
	 * 
	 * 1. Simplicité du Code : Le pattern "State" rend le code plus lisible en
	 * déplaçant la logique conditionnelle liée à l'état dans des classes d'état
	 * distinctes.
	 * 
	 * 2. Maintenabilité : Le code devient plus modulaire et plus facile à maintenir
	 * grâce à la séparation de la logique associée à chaque état dans des classes
	 * distinctes.
	 * 
	 * 3. Extensibilité : L'ajout de nouveaux états est simplifié, sans affecter le
	 * code existant.
	 * 
	 * 4. Élimination des Blocs Switch : Le pattern "State" permet d'éliminer les
	 * blocs de commutation souvent présents dans le code.
	 * 
	 * 5. Facilité de Transition : Les transitions d'état sont gérées de manière
	 * centralisée dans le contexte, simplifiant la gestion des changements d'état.
	 * 
	 * 6. Réutilisation : Les classes d'état peuvent être réutilisées dans d'autres
	 * contextes, favorisant la réutilisabilité du code.
	 * 
	 * 7. Conformité avec le Principe d'Open/Closed : Ajouter de nouveaux états
	 * n'affecte pas les classes existantes, respectant le principe d'Open/Closed.
	 *
	 */
	
	public static void main(String[] args) {

		MediaPlayer mediaPlayer = new MediaPlayer();
		
		mediaPlayer.play();
		mediaPlayer.play();
		mediaPlayer.pause();
		mediaPlayer.play();
		mediaPlayer.stop();
		mediaPlayer.pause();
		
		mediaPlayer.setState(new CrashedState());
		
		mediaPlayer.play();
	}
}
