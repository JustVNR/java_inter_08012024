package fr.dawan.comportement.state;

public class CrashedState implements State{

	@Override
	public void play() {

		System.out.println("Sorry I have crashed.");
	}

	@Override
	public void pause() {

		System.out.println("Sorry I have crashed.");
	}

	@Override
	public void stop() {

		System.out.println("Sorry I have crashed.");
	}
}
