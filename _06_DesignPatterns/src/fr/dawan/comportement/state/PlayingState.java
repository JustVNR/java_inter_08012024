package fr.dawan.comportement.state;

public class PlayingState implements State{

	@Override
	public void play() {

		System.out.println("Already playing");
	}

	@Override
	public void pause() {

		System.out.println("Pausing the player");
	}

	@Override
	public void stop() {

		System.out.println("Stopping the playing");
	}
}
