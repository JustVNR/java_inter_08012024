package fr.dawan.comportement.state;

public interface State {

	void play();
	
	void pause();
	
	void stop();
}
