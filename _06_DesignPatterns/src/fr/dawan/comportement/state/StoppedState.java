package fr.dawan.comportement.state;

public class StoppedState implements State{

	@Override
	public void play() {

		System.out.println("Starting playback");
	}

	@Override
	public void pause() {

		System.out.println("Cannot pause, player is stopped");
	}

	@Override
	public void stop() {

		System.out.println("Player is already stopped");
	}
}
