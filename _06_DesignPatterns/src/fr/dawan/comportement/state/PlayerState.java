package fr.dawan.comportement.state;

public enum PlayerState {
	PLAYING, PAUSED, STOPPED, CRASHED
}
