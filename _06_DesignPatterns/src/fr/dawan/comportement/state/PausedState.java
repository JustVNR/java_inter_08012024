package fr.dawan.comportement.state;

public class PausedState implements State{

	@Override
	public void play() {

		System.out.println("Resuming playback");
	}

	@Override
	public void pause() {

		System.out.println("Already paused");
	}

	@Override
	public void stop() {

		System.out.println("Stopping the playing");
	}
}