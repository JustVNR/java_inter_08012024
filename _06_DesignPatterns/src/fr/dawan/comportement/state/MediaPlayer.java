package fr.dawan.comportement.state;

public class MediaPlayer {

	private State state;

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public MediaPlayer() {

		this.state = new StoppedState(); // le lecteur est créé arrêté
	}

	public void play() {

		state.play();

		this.setState(new PlayingState());
	}

	public void pause() {

		state.pause();

		this.setState(new PausedState());
	}

	public void stop() {

		state.stop();

		this.setState(new StoppedState());
	}
}
