package fr.dawan.comportement.iterator;

public class App {

	/*
	 * Le pattern iterator fournit un moyen de parcourir séquentiellement les
	 * éléméents d'une collection sans exposer les détails concernant sa structure
	 * interne.
	 * 
	 * Principes fondamentaux :
	 * 
	 * - Encapsulation : Encapsule les détails de l'implémentation de la collection
	 * et on fournit simplement un moyen standart de la parcourir
	 * 
	 * - Indépendance de l'inerface client : les utilisateurs n'ont pas besoin de
	 * connaitre la structure interne de la collection pour la parcourir
	 */
	public static void main(String[] args) {

		CollectionLivres livres = new CollectionLivres();

		livres.ajouterLivre("Livre A");
		livres.ajouterLivre("Livre B");
		livres.ajouterLivre("Livre C");

		MyIterator iterator = livres.creerIterator();

		while (iterator.hasNext()) {

			String titreLivre = iterator.suivant();

			System.out.println(titreLivre);
		}

		System.out.println("\n=============== RAPPEL OPERATEUR D'INCREMENTATION ==================\n");

		int i = 0;

		// POST INCREMENTATION
		System.out.println(i++); // 0 car i est incrémenté après affichage

		// PRE INCREMENTATION
		System.out.println(++i); // 2 car i valait 1 après avoir été incrémenté ci dessus et est cette fois
									// incrémenté avant affichage
	}
}
