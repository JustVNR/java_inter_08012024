package fr.dawan.comportement.iterator;

import java.util.ArrayList;
import java.util.List;

public class CollectionLivres {

	private List<String> livres;

	public CollectionLivres() {
		super();
		this.livres = new ArrayList<String>();
	}

	public void ajouterLivre(String titre) {
		livres.add(titre);
	}

	public MyIterator creerIterator() {
		return new IteratorLivres();
	}

	private class IteratorLivres implements MyIterator {

		private int position;

		@Override
		public boolean hasNext() {
			return position < livres.size();
		}

		@Override
		public String suivant() {

			if (hasNext()) {
				return livres.get(position++); 
			}
			return null;
		}
	}
}
