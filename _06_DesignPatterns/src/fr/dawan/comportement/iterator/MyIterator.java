package fr.dawan.comportement.iterator;

public interface MyIterator {

	boolean hasNext();

	String suivant(); // On pourrait faire un iterateur générique
						// auquel cas la méthode suivant pourrait retourner n'importe quel type (pas
						// necessairement un String)
}
