package fr.dawan.comportement.template_method;

public class Circle extends Shape {

	private double radius;
	
	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	@Override
	protected double calulateArea() {
		
		return Math.PI * radius * radius;
	}

	@Override
	protected void displaySpecificShape() {
		System.out.println("Rayon du cercle : " + radius);
	}

	@Override
	protected String getShapeType() {

		return "Circle";
	}
}
