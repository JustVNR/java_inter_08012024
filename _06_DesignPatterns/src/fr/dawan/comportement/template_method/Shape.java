package fr.dawan.comportement.template_method;

public abstract class Shape {

	public final void displayShape() {
		
		System.out.println("Affichage de la forme : " + getShapeType());
		
		displaySpecificShape();
		
		System.out.println("Superficie : " + calulateArea());
	}

	protected abstract double calulateArea();

	protected abstract void displaySpecificShape();

	protected abstract String getShapeType();
}
