package fr.dawan.comportement.template_method;

public class The extends BoissonChaude{

	@Override
	protected void ajouterCondiments() {
		System.out.println("Ajouter du citron");
	}

	@Override
	protected void infuser() {
		System.out.println("Infuser le thé");
	}
}
