package fr.dawan.comportement.template_method;

public abstract class BoissonChaude {

	// "template Method" définissant l'algoritme général
	// le mot clé 'final' interdit la redéfinition de la template méthod dans les classes filles
	final void preparerBoissonChaude() {
		
		faireBouillirEau();
		
		infuser();
		
		verserEauDansTasse();
		
		ajouterCondiments();
	}

	private void verserEauDansTasse() {
		
		System.out.println("Verser dans la tasse");
	}

	private void faireBouillirEau() {
		
		System.out.println("Faire bouillir l'eau");
	}
	
	// Méthodes abstraites qui devront être implémentées par des classes spécialisées
	
	protected abstract void ajouterCondiments();

	protected abstract void infuser();
}
