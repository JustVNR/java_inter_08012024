package fr.dawan.comportement.template_method;

public class App {

	/*
	 * Le pattern Template Method définit la structure d'un algorithme dans une
	 * (template) méthode.
	 * 
	 * Mais laisse le soin d'implémenter certaines étapes spécifiques de
	 * l'algorithme à des classes spécialisées.
	 */
	public static void main(String[] args) {

		BoissonChaude the = new The();

		BoissonChaude cafe = new Cafe();

		System.out.println("Préparation du thé");
		the.preparerBoissonChaude();

		System.out.println("===================");

		System.out.println("Préparation du café");
		cafe.preparerBoissonChaude();

		System.out.println("\n======== SHAPES ==========\n");
		// Exemple 2 SHAPES

		/*
		 * Créer un programme permettant de modéliser différents types de formes
		 * géométriques telles que cercles, rectangles et triangles.
		 * 
		 * Chaque forme doit pouvoir être affichée et calculer sa superficie.
		 * 
		 * Utilisez le pattern de "Template Method" pour :
		 * 
		 * - créer une classe abstraite Shape exposant une méthode 'displayShape'
		 * 
		 * - créez des classes concrètes pour chaque type de forme qui implémentent ces
		 * méthodes.
		 */
		Shape circle = new Circle(5);

		Shape rectangle = new Rectangle(4, 6);

		Shape triangle = new Triangle(3, 8);

		circle.displayShape();
		System.out.println("--------------");
		rectangle.displayShape();
		System.out.println("--------------");
		triangle.displayShape();

//        RESULTAT ATTENDU :

//        Affichage de la forme : Cercle
//        Rayon du cercle : 5.0
//        Superficie : 78.53981633974483
//        --------------
//        Affichage de la forme : Rectangle
//        Longueur du rectangle : 4.0
//        Largeur du rectangle : 6.0
//        Superficie : 24.0
//        --------------
//        Affichage de la forme : Triangle
//        Base du triangle : 3.0
//        Hauteur du triangle : 8.0
//        Superficie : 12.0
	}

}
