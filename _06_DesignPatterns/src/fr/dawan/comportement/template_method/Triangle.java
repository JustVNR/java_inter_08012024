package fr.dawan.comportement.template_method;

public class Triangle extends Shape {

	private double base;
	private double height;

	public Triangle(double base, double height) {
		super();
		this.base = base;
		this.height = height;
	}

	@Override
	protected double calulateArea() {

		return 0.5 * base * height;
	}

	@Override
	protected void displaySpecificShape() {

		System.out.println("Base du triangle :" + base);

		System.out.println("Hauteur du rectangle :" + height);
	}

	@Override
	protected String getShapeType() {

		return "Triangle";
	}
}
