package fr.dawan.comportement.template_method;

public class Cafe extends BoissonChaude{

	@Override
	protected void ajouterCondiments() {
		System.out.println("Ajouter du sucre et du lait");
		
	}

	@Override
	protected void infuser() {
		System.out.println("Infuser le café moulu");
	}
}
