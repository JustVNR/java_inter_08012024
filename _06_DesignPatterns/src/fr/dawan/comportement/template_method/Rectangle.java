package fr.dawan.comportement.template_method;

public class Rectangle extends Shape {

	
	private double length;
	
	private double width;
		
	public Rectangle(double length, double width) {
		super();
		this.length = length;
		this.width = width;
	}

	@Override
	protected double calulateArea() {
		
		return length * width;
	}

	@Override
	protected void displaySpecificShape() {

		System.out.println("Longueur du rectangle :" + length);
		
		System.out.println("Largeur du rectangle :" + width);
	}

	@Override
	protected String getShapeType() {
		
		return "Rectangle";
	}

}
