package fr.dawan.comportement.strategy;

public class StripeStrategy implements IPaymentStrategy{

	@Override
	public void pay(double amount) {

		System.out.println("Paiement par stripe de " + amount + " euros");
	}
}