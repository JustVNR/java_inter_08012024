package fr.dawan.comportement.strategy;

public class TriService implements ITriService {

	private IStrategyTri strategyTri;
	
	public IStrategyTri getStrategyTri() {
		return strategyTri;
	}

	public TriService(IStrategyTri strategyTri) {
		super();
		this.strategyTri = strategyTri;
	}

	@Override
	public void setStrategyTri(IStrategyTri strategyTri) {
		
		this.strategyTri = strategyTri;
	}

	@Override
	public void effectuerTrie(int[] tableau) {
		
		strategyTri.trier(tableau);
	}
}
