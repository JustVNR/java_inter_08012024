package fr.dawan.comportement.strategy;

public interface IStrategyTri {

	void trier(int[] tableau);
}
