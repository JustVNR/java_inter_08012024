package fr.dawan.comportement.strategy;

public interface IPaymentStrategy {

	void pay(double amount);
}
