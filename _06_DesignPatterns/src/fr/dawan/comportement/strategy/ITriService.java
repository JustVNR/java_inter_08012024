package fr.dawan.comportement.strategy;

public interface ITriService {

	void setStrategyTri(IStrategyTri strategyTri);
	
	void effectuerTrie(int[] tableau);
}
