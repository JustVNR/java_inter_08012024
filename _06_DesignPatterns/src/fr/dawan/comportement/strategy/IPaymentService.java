package fr.dawan.comportement.strategy;

public interface IPaymentService {

	public void makePayment(double amount);
	
	void setPaymentStrategy(IPaymentStrategy paymentStrategy);
}
