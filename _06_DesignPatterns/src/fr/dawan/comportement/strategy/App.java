package fr.dawan.comportement.strategy;

public class App {

	/*
	 * le pattern Strategy définit une famille d'algorithmes, les encapsule, et les
	 * rends interchangeables.
	 */
	public static void main(String[] args) {

		ITriService triService = new TriService(new Strategy1());

		int[] tableau = { 4, 2, 5, 1, 9 };
		
		triService.effectuerTrie(tableau);
		
		triService.setStrategyTri(new Strategy2());
		
		triService.effectuerTrie(tableau);
		
		// EXEMPLE 2 : PAIEMENT
		
		IPaymentService paymentService = new PaymentService(new CreditCardStrategy());
        paymentService.makePayment(50.0);

        // Paiement via Stripe
        // IPaymentService paiementStripe = new PaymentService(new StripeStrategy());
        paymentService.setPaymentStrategy(new StripeStrategy());
        
        paymentService.makePayment(30.0);
        
        // Paiement par carte bancaire de 50.0 euros
        // Paiement par stripe de 30.0 euros
        
	}
}
