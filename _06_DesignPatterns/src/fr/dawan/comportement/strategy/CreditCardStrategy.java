package fr.dawan.comportement.strategy;

public class CreditCardStrategy implements IPaymentStrategy{

	@Override
	public void pay(double amount) {

		System.out.println("Paiement par carte bancaire de " + amount + " euros");
	}
}
