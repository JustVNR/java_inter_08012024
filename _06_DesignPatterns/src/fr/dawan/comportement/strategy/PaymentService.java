package fr.dawan.comportement.strategy;

public class PaymentService implements IPaymentService{

	private IPaymentStrategy paymentStrategy;
	
	public IPaymentStrategy getPayementStrategy() {
		return paymentStrategy;
	}

	public PaymentService(IPaymentStrategy paymentStrategy) {
		super();
		this.paymentStrategy = paymentStrategy;
	}

	@Override
	public void makePayment(double amount) {

		paymentStrategy.pay(amount);
	}

	@Override
	public void setPaymentStrategy(IPaymentStrategy paymentStrategy) {

		this.paymentStrategy = paymentStrategy;
	}
}
