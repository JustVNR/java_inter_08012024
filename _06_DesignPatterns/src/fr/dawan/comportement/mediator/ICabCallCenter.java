package fr.dawan.comportement.mediator;

public interface ICabCallCenter {

	void register(ICab cab);
	
	void bookCab(IPassenger passenger);
}
