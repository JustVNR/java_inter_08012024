package fr.dawan.comportement.mediator;

public class User implements IUser{

	private String name;
	
	private IChatMediator mediator;

	public void setName(String name) {
		this.name = name;
	}

	public User(String name, IChatMediator mediator) {
		super();
		this.name = name;
		this.mediator = mediator;
		mediator.register(this); // enregistre l'utilisateur courant dans le médiateur
	}


	@Override
	public void sendMessage(String message) {
		mediator.sendMessage(message, this); // l'utilisateur envoie le message par l'intermédiaire du mediator
		
	}

	@Override
	public void receiveMessage(String message) {

		System.out.println(this.getName() + " reçoit le message : '" + message + "'");
	}

	@Override
	public String getName() {
		
		return this.name;
	}
}
