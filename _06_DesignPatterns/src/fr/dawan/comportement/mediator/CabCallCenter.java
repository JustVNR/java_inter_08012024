package fr.dawan.comportement.mediator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CabCallCenter implements ICabCallCenter{

	// Map : collection de type dictionnaire (et donc de type clé/valeur)
	
	private static Map<String, ICab> cabs = new HashMap<>();
	
	@Override
	public void register(ICab cab) {

		if(!cabs.containsValue(cab)) {
			cabs.put(cab.getName(), cab); // On utilise le nom du taxi en tant que clé
		}
	}

	@Override
	public void bookCab(IPassenger passenger) {

		ICab closestFreeCab = findClosesFreeCab(passenger);
		
		if(closestFreeCab == null) {
			
			passenger.acknoledge("No cab available yet");
			
			return;
		}
		
		closestFreeCab.assign(passenger.getName(), passenger.getAddress());
		
		passenger.acknoledge(closestFreeCab.getName());
	}

	private ICab findClosesFreeCab(IPassenger passenger) {

		List<ICab> freeCabs = cabs.values().stream().filter(c -> c.isFree()).toList();
		
		if(freeCabs.size() == 0) {
			return null;
		}
		
		ICab closestCab = freeCabs.get(0);
		
		double closestDistance = getDistance(closestCab.getCurrentLocation(), passenger.getLocation());
		
		for (ICab cab : freeCabs) {
			
			double currentDistance = getDistance(cab.getCurrentLocation(), passenger.getLocation());
			
			if(currentDistance < closestDistance) {
				
				closestDistance = currentDistance;
				
				closestCab = cab;
			}
		}
		
		return closestCab;
	}

	private double getDistance(int cabLocation, int passengerLocation) {
		
		return Math.abs(cabLocation - passengerLocation);
	}
}
