package fr.dawan.comportement.mediator;

public interface IUser {

	void sendMessage(String message);
	
	void receiveMessage(String message);
	
	String getName();
}
