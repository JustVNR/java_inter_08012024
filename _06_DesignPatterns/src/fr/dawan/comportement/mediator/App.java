package fr.dawan.comportement.mediator;

public class App {

	/*
	 * le pattern Mediator défint un objet (le médiateur) qui centralise la
	 * communication entre plusieurs objets.
	 * 
	 * Lesdits objets objets ne communiquenent donc pas directement les uns avec les
	 * autres, mais par l'intermédiaire du médiateur.
	 * 
	 * Principes fondamentaux :
	 * 
	 * - Communication indirecte
	 * 
	 * - Réduction du couplage entre les objets
	 * 
	 * - Centralisation de la logique de communication
	 */
	public static void main(String[] args) {

		System.out.println("\n============ CHAT ===============\n");
		
		IChatMediator mediator = new ChatMediator();
		
		IUser riri = new User("riri", mediator);
		IUser fifi = new User("fifi", mediator);
		IUser loulou = new User("loulou", mediator);
		
		riri.sendMessage("Salut ça va ?");
		loulou.sendMessage("ça va bien merci !");
		
		System.out.println("\n============ CABS ===============\n");
		
		ICabCallCenter callCenter = new CabCallCenter();
		
		IPassenger p1 = new Passenger("passenger 1", "123 street", 10);
		IPassenger p2 = new Passenger("passenger 2", "456 street", 25);
		
		ICab cab1 = new Cab("Cab1", 11, true);
		ICab cab2 = new Cab("Cab2", 22, false);
		
		callCenter.register(cab1);
		callCenter.register(cab2);
		
		callCenter.bookCab(p1);
		callCenter.bookCab(p2);
	}
}
