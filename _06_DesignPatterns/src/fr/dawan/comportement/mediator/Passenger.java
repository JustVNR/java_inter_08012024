package fr.dawan.comportement.mediator;

public class Passenger implements IPassenger{

	private String name;
	
	private String address;
	
	private int location; // En vrai on utiliserait les coordonnées GPS
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public Passenger(String name, String address, int location) {
		super();
		this.name = name;
		this.address = address;
		this.location = location;
	}

	@Override
	public void acknoledge(String cabName) {

		System.out.println("Passenger " + this.name + ", Cab : " + cabName);
	}
}
