package fr.dawan.comportement.mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediator implements IChatMediator {

	private List<IUser> users = new ArrayList<IUser>();

	@Override
	public void sendMessage(String message, IUser user) {

		for (IUser u : users) {
			if (u.getName() != user.getName())
				u.receiveMessage(message); // On envoie le message à tous les inscrits sauf l'emetteur
		}
	}

	@Override
	public void register(IUser user) {

		users.add(user);
	}
}
