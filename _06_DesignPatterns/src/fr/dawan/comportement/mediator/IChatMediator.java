package fr.dawan.comportement.mediator;

public interface IChatMediator {

	void sendMessage(String message, IUser user);
	
	void register(IUser user);
}
