package fr.dawan.comportement.mediator;

public class Cab implements ICab{

	private String name;
	
	private int currentLocation; // En vrai on utiliserait les coordonnées GPS
	
	private boolean isFree;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(int currentLocation) {
		this.currentLocation = currentLocation;
	}

	public boolean isFree() {
		return isFree;
	}

	public void setFree(boolean isFree) {
		this.isFree = isFree;
	}

	public Cab(String name, int currentLocation, boolean isFree) {
		super();
		this.name = name;
		this.currentLocation = currentLocation;
		this.isFree = isFree;
	}

	@Override
	public void assign(String passenGerName, String address) {

		System.out.println("Cab " + this.name + " assigned to passenger :" + passenGerName);
		
		setFree(false);
	}
}
