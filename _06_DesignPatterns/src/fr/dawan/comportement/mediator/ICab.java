package fr.dawan.comportement.mediator;

public interface ICab {

	void assign(String name, String address);
	
	String getName();
	
	int getCurrentLocation();
	
	boolean isFree();
}
