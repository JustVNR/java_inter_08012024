package fr.dawan.comportement.mediator;

public interface IPassenger {

	void acknoledge(String name);
	
	String getName();
	
	String getAddress();
	
	int getLocation();
}
