package fr.dawan.comportement.chainOfResponsability;

public class Complaint {

	private int studentId;
	
	private ComplaintType type;
	
	private ComplaintState state;
	
	private String message;

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public ComplaintType getType() {
		return type;
	}

	public void setType(ComplaintType type) {
		this.type = type;
	}

	public ComplaintState getState() {
		return state;
	}

	public void setState(ComplaintState state) {
		this.state = state;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	public Complaint(int studentId, ComplaintType type, ComplaintState state, String message) {
		super();
		this.studentId = studentId;
		this.type = type;
		this.state = state;
		this.message = message;
	}

}
