package fr.dawan.comportement.chainOfResponsability;

public abstract class Staff {

	private String name;
	
	private Staff successor;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Staff getSuccessor() {
		return successor;
	}

	public void setSuccessor(Staff successor) {
		this.successor = successor;
	}
	
	public Staff(String name, Staff successor) {
		super();
		this.name = name;
		this.successor = successor;
	}
	
	public abstract void handleComplaint(Complaint req);
}
