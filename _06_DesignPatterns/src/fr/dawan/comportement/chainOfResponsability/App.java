package fr.dawan.comportement.chainOfResponsability;

public class App {

	/*
	 * Le pattern "Chain Of Responsability" permet de traiter une requête à travers
	 * une chaine de gestionnaires, chacun d'entre eux décidant s'il peut traiter
	 * la requête ou s'il doit la transmettre au gestionnaire suivant dans la chaine
	 * de responsabilité.
	 * 
	 * Avantages :
	 * 
	 * - Decompose la logique de traitement en petits gestionnaires indépendants -
	 * Permet d'ajouter ou de retirer des gestionnaires sans impacter le code
	 * existant
	 * 
	 * Inconvénients :
	 * 
	 * - La requête peut ne pas être traitée si elle atteint la fin de la chaine
	 * sans qu'aucun gestionnaire ne puisse la traiter
	 * 
	 * - Les développeurs doivent s'assurer que chaque gestionnaire transmet
	 * correctement la requête au suivant.
	 */
	public static void main(String[] args) {

		Teacher t = new Teacher("prof", new HeadTeacher("Responsable pedago", new Director("Directeur", null)));

		t.handleComplaint(new Complaint(12, ComplaintType.PROF, ComplaintState.OPENED, "req1"));
		System.out.println("===============================");

		t.handleComplaint(new Complaint(123, ComplaintType.PEDAGO, ComplaintState.OPENED, "req2"));
		System.out.println("===============================");
		t.handleComplaint(new Complaint(1234, ComplaintType.DIRLO, ComplaintState.OPENED, "req3"));
	}
}
