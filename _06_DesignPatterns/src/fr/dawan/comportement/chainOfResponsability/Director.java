package fr.dawan.comportement.chainOfResponsability;

public class Director extends Staff{

	public Director(String name, Staff successor) {
		super(name, successor);
	}

	@Override
	public void handleComplaint(Complaint req) {
		
		System.out.println("Requête traitée par le directeur...");
		req.setState(ComplaintState.CLOSED);
	}
}
