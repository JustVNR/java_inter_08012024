package fr.dawan.comportement.chainOfResponsability;

public class Teacher extends Staff{

	public Teacher(String name, Staff successor) {
		super(name, successor);
	}

	@Override
	public void handleComplaint(Complaint req) {
		
		if(req.getType() == ComplaintType.PROF) {
			
			System.out.println("Requête traitée par le prof...");
			req.setState(ComplaintState.CLOSED);
		}
		else if(getSuccessor() != null) {
			
			System.out.println("Requête transmise au directeur pédagogique...");
			getSuccessor().handleComplaint(req);
		}
	}
}
