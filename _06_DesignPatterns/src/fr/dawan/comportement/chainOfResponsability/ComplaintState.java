package fr.dawan.comportement.chainOfResponsability;

public enum ComplaintState {
	OPENED, CLOSED
}
