package fr.dawan.comportement.chainOfResponsability;

public class HeadTeacher extends Staff {

	public HeadTeacher(String name, Staff successor) {
		super(name, successor);
	}

	@Override
	public void handleComplaint(Complaint req) {

		// L'objet courant décide selon la logique métier de l'appli s'il doit traiter
		// la requête lui-même ou la passer à son successeur
		if (req.getType() == ComplaintType.PEDAGO) {

			System.out.println("Requête traitée par le responsable pédagogique...");
			req.setState(ComplaintState.CLOSED);
		} else if (getSuccessor() != null) {

			System.out.println("Requête transmise au directeur...");
			getSuccessor().handleComplaint(req);
		}
	}
}