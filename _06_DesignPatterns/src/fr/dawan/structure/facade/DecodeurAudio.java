package fr.dawan.structure.facade;

public class DecodeurAudio {

	public void decoder(String fichier) {
		System.out.println("Decodage du fichier : " + fichier);
	}
}
