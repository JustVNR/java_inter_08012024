package fr.dawan.structure.facade;

public class App {

	/*
	 * Le pattern Facade fournit une interface unifiée / simplifié à un ensemble
	 * complexe de classes.
	 * 
	 * Il définit une interface de niveau supérieur qui facilite l'utilisation d'un
	 * systeme complexe.
	 * 
	 * Il propose donc fonctionalités plus limitées.
	 */
	public static void main(String[] args) {

		LecteurAudioFacade lecteurAudio = new LecteurAudioFacade();

		lecteurAudio.jouer("musique.mp3"); // l'utilisateur a juste à appeler une méthode de la Facde sans avoir à se
											// soucier de son implémentation.
	}
}
