package fr.dawan.structure.facade;

public class ChargeurFichier {

	public void chargerFichier(String fichier) {
		System.out.println("Chargement du fichier : " + fichier);
	}
}
