package fr.dawan.structure.composite;

import java.util.ArrayList;
import java.util.List;

public class DirectoryItem extends FileSystemItem{

	private List<FileSystemItem> items;
	
	
	public DirectoryItem(String name) {
		super(name);
		items = new ArrayList<FileSystemItem>();
	}

	@Override
	public int getCount() {
		
		return items.size();
	}

	public void addItem(FileSystemItem item) {
		items.add(item);
	}
	
	public void removeItem(FileSystemItem item) {
		items.remove(item);
	}
}
