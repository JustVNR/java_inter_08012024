package fr.dawan.structure.composite;

public class App {

	/*
	 * Le pattern Composite permet d'agencer les objets dans des arborescences afin
	 * de pouvoir traiter ces arborescences comme des objets individuels.
	 * 
	 * Le but est de pouvoir traiter de la même manière :
	 * 
	 * - les objets individuels (ex : un simple fichier)
	 * 
	 * - et des compositions d'objets (composites) (ex : un dossier contenant de
	 * simples fichiers et des sous dossiers)
	 */
	public static void main(String[] args) {

		DirectoryItem root = new DirectoryItem("root");
		DirectoryItem subDir1 = new DirectoryItem("folder1");
		DirectoryItem subDir2 = new DirectoryItem("folder2");
		
		FileItem file1 = new FileItem("file1");
		FileItem file2 = new FileItem("file2");
		FileItem file3 = new FileItem("file3");
		FileItem file4 = new FileItem("file4");
		
		// On peut traiter les fichiers et les dossiers de la même manière
		subDir1.addItem(file1);
		subDir1.addItem(file2);
		root.addItem(file3);
		subDir2.addItem(file4);
		subDir1.addItem(subDir2);
		
		System.out.println(root.getCount());
		System.out.println(subDir1.getCount());
	}

}
