package fr.dawan.structure.composite;

public abstract class FileSystemItem {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FileSystemItem(String name) {
		super();
		this.name = name;
	}
	
	public abstract int getCount();
}
