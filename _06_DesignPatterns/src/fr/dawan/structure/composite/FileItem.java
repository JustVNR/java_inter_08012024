package fr.dawan.structure.composite;

public class FileItem extends FileSystemItem{

	public FileItem(String name) {
		super(name);
	}

	@Override
	public int getCount() {
		return 1;
	}
}
