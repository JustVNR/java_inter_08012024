package fr.dawan.structure.decorator;

public class Salad extends IngredientDecorator {

	public Salad(IKebab newKebab) {
		super(newKebab);
		System.out.println("Ajout de salade");
	}
	
	@Override
	public String getDescription() {
		return newKebab.getDescription() + ", Salade";
	}
	
	@Override
	public double getPrice() {
		return newKebab.getPrice() + 1.5;
	}
}
