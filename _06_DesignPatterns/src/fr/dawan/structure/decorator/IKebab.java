package fr.dawan.structure.decorator;

public interface IKebab {

	String getDescription();
	
	double getPrice();
}
