package fr.dawan.structure.decorator;

public class Tomat extends IngredientDecorator {

	public Tomat(IKebab newKebab) {
		super(newKebab);
		System.out.println("Ajout de tomate");
	}
	
	@Override
	public String getDescription() {
		return newKebab.getDescription() + ", Tomate";
	}
	
	@Override
	public double getPrice() {
		return newKebab.getPrice() + 2.3;
	}

}
