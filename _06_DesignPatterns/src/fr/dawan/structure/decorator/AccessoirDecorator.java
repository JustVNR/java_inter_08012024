package fr.dawan.structure.decorator;

public abstract class AccessoirDecorator implements ICostume{

	protected ICostume newCostume;

	public AccessoirDecorator(ICostume newKebab) {
		super();
		this.newCostume = newKebab;
	}
	
	@Override
	public String toString() {
		return getDescription() + " coute " + getPrice() + "€.";
	}
}
