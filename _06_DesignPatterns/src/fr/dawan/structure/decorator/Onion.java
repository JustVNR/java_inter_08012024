package fr.dawan.structure.decorator;

public class Onion extends IngredientDecorator {

	public Onion(IKebab newKebab) {
		super(newKebab);
		System.out.println("Ajout d'onion");
	}
	
	@Override
	public String getDescription() {
		return newKebab.getDescription() + ", Onion";
	}
	
	@Override
	public double getPrice() {
		return newKebab.getPrice() + 1.2;
	}
}
