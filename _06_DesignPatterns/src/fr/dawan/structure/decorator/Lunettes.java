package fr.dawan.structure.decorator;

public class Lunettes extends AccessoirDecorator {

	public Lunettes(ICostume newCostume) {
		super(newCostume);
		System.out.println("Ajout de lunettes");
	}
	
	@Override
	public String getDescription() {
		return newCostume.getDescription() + ", Lunettes";
	}
	
	@Override
	public double getPrice() {
		return newCostume.getPrice() + 5.5;
	}
}
