package fr.dawan.structure.decorator;

public abstract class IngredientDecorator implements IKebab{

	protected IKebab newKebab;

	public IngredientDecorator(IKebab newKebab) {
		super();
		this.newKebab = newKebab;
	}
	
	@Override
	public String toString() {
		return getDescription() + " coute " + getPrice() + "€.";
	}
}
