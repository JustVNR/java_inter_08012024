package fr.dawan.structure.decorator;

public interface ICostume {
	String getDescription();

	double getPrice();
}
