package fr.dawan.structure.decorator;

public class Moustache extends AccessoirDecorator {

	public Moustache(ICostume newCostume) {
		super(newCostume);
		System.out.println("Ajout de moustaches");
	}
	
	@Override
	public String getDescription() {
		return newCostume.getDescription() + ", Moustaches";
	}
	
	@Override
	public double getPrice() {
		return newCostume.getPrice() + 3.5;
	}
}
