package fr.dawan.structure.decorator;

public class BaseKebab implements IKebab{

	@Override
	public String getDescription() {
		
		return "nouveau Kebab";
	}

	@Override
	public double getPrice() {
		
		return 3.0;
	}
}
