package fr.dawan.structure.decorator;

public class BaseCostume implements ICostume{

	@Override
	public String getDescription() {
		return "nouveau Costume";
	}

	@Override
	public double getPrice() {
		
		return 10.0;
	}

}
