package fr.dawan.structure.decorator;

public class Perruque extends AccessoirDecorator {

	public Perruque(ICostume newCostume) {
		super(newCostume);
		System.out.println("Ajout de perruque");
	}
	
	@Override
	public String getDescription() {
		return newCostume.getDescription() + ", Perruque";
	}
	
	@Override
	public double getPrice() {
		return newCostume.getPrice() + 5.5;
	}
}
