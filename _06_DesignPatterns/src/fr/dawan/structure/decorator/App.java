package fr.dawan.structure.decorator;

public class App {

	/*
	 * Le pattern Decorator permet de créer / modifier / décorer un objet
	 * dynamiquement (en temps réel)
	 * 
	 * Cela permet par exemple de créer en temps réel n'importe quel type de Kebab
	 * sur base des ingrédents disponibles. Sans avoir à prévoir à l'avance toutes
	 * les recettes possibles.
	 * 
	 */
	public static void main(String[] args) {

		IKebab kebab = new Onion(new Tomat(new Salad(new BaseKebab())));

		System.out.println(kebab);
		
		// EXEMPLE 2 : COSTUME

		ICostume costume = new Lunettes(new Perruque(new BaseCostume()));

		System.out.println(costume);
		
		// RESULTAT ATTENDU
//		Nouveau costume
//		Ajout perruque
//		Ajout lunettes
//		costume , Perruque, Lunettes coute 22.0€.
	}
}
