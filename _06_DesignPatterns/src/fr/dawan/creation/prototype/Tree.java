package fr.dawan.creation.prototype;

public class Tree implements Cloneable{

	private String color;
	
	private float size;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}

	
	public Tree() {
		super();
		
	}
	
	public Tree(String color, float size) {
		super();
		this.color = color;
		this.size = size;
	}

	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	@Override
	public String toString() {
		return "Tree [color=" + color + ", size=" + size + "]";
	}
}
