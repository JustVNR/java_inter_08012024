package fr.dawan.creation.prototype;

import java.util.ArrayList;
import java.util.List;

public class Forest implements Cloneable {

	private List<Tree> trees;

	public List<Tree> getTrees() {
		return trees;
	}

	public void setTrees(List<Tree> trees) {
		this.trees = trees;
	}

	public Forest() {
		super();
		this.trees = new ArrayList<Tree>();
	}

	public Forest(List<Tree> trees) {
		super();
		this.trees = trees;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// return super.clone();

		// PROBLEME : la méthode super.clone() ne clone pas les listes
		Forest f = (Forest) super.clone();

		f.trees = new ArrayList<Tree>();

		for (Tree tree : trees) {
			f.trees.add((Tree) tree.clone());
		}

		return f;
	}

	@Override
	public String toString() {

		String result = "";
		
		for (Tree tree : trees) {
			result += tree.toString() + "\n";
		}
		
		return result;
	}
}
