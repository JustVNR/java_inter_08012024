package fr.dawan.creation.prototype;

public class App {

	/*
	 * Le Pattern prototype vise à créer de nouveaux objets en clonant l'instance d'une classe prototype.
	 * 
	 * 
	 */
	public static void main(String[] args) {
		
		try {
			Tree treeProto = new Tree("green", 10.5F);
			
			Tree treeClone = (Tree) treeProto.clone();
			
			treeClone.setSize(5.5F);
			
			System.out.println(treeProto + "(proto)");
			System.out.println(treeClone + "(clone)");
			
			System.out.println("\n============= FOREST ==============\n");
			
			Forest forestProto = new Forest();
			
			forestProto.getTrees().add(treeProto);
			forestProto.getTrees().add(treeClone);
			forestProto.getTrees().add(treeProto);
			
			Forest forestClone = (Forest) forestProto.clone();
			forestClone.getTrees().add(treeClone);
			
			System.out.println("\nForêt originale : \n");
			System.out.println(forestProto);
			
			System.out.println("\nForêt clonée : \n");
			System.out.println(forestClone);
		}
		catch(CloneNotSupportedException e ) {
			e.printStackTrace();
		}
	}
}
