package fr.dawan.creation.singleton;

public class App {

	/*
	 * Le Singleton est un pattern dont l'objectif est de restreindre
	 * l'instanciation d'une classe à un seul objet.
	 * 
	 * Il est utilisé lorsqu'on a besoin d'un objet pour coordonner des opérations
	 * dans un système.
	 */
	public static void main(String[] args) {

		Pdg pdg1 = Pdg.getInstance("pdg1");
		Pdg pdg2 = Pdg.getInstance("pdg2");
		
		System.out.println(pdg1.getName());
		System.out.println(pdg2.getName()); // les 2 variables pdg1 et pdg2 pointent vers le même objet. 
	}

}
