package fr.dawan.creation.singleton;

public class Pdg {

	private String name;
	
	// le mot clé "volatile" empêche les effets de bords dans le cas d'une appli multi threadée.
	private static volatile Pdg instance = null;
	
	// La présence d'un constructeur privé enpeche d'instancier la classe directement depuis le monde extérieur.
	private Pdg() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// Synchronized permet de verouiller l'accès à la méthode en cas d'appli multi thread
	public final synchronized static Pdg getInstance(String name) {
		
		if(instance == null) {
			
			instance = new Pdg();
		}
		
		instance.name = name;
		
		return instance;
	}
}
