package fr.dawan.creation.builder;

public class User {

	private String firstName;
	private String lastName;
	private int age;
	private String phone;
	private String address;

	// Option 1 : Sans le patter Builder on pourrait être tentés de définir de
	// nombreux onstructeurs :

	// User(firstName, lastName)
	// User(firstName, lastName, age)
	// User(firstName, lastName, phone)
	// User(firstName, lastName, age, phone)
	// ...

	// Option 2 : Utiliser une classe gérant la création d'objets de type User

	private User() {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static class UserBuilder {

		private User user;

		public UserBuilder(String firstName, String lastName) {

			this.user = new User();
			this.user.firstName = firstName;
			this.user.lastName = lastName;
		}

		// Méthodes permettant d'ajouter les attributs optionnels

		public UserBuilder age(int age) {
			user.age = age;
			return this; // permettre de chainer les appels des méthodes age(), phone(),...
		}

		public UserBuilder phone(String phone) {
			user.phone = phone;
			return this;
		}

		public UserBuilder address(String address) {
			user.address = address;
			return this;
		}

		// Méthode Build appelée lors de l'appel au constructeur

		public User build() {

			return user;
		}
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + ", phone=" + phone
				+ ", address=" + address + "]";
	}
}
