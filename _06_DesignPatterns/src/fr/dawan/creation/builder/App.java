package fr.dawan.creation.builder;

public class App {

	/*
	 * Le pattern Builder vise à faciliter la construction d'objets complexes en
	 * évitant d'avoir à leur associer un (top) grnad nombre de constructeurs.
	 */
	
	public static void main(String[] args) {

		User user = new User
				.UserBuilder("riri", "duck")
				.age(12)
				.address("123 rue des canards")
				.build();
		
		User user2 = new User.UserBuilder("fifi", "duck")
				.age(12)
				.phone("06123456987")
				.build();
		
		System.out.println(user);
		
		System.out.println(user2);
	}
	
}
