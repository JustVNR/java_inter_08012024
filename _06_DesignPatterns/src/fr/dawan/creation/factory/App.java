package fr.dawan.creation.factory;

public class App {

	/*
	 * Le pattern Factory permet de créer des objets sans exposer leur logique de
	 * création à l'utilisateur, et retourne les objets ainsis créés sous forme
	 * d'abstractions (classe abstraite ou interface)
	 */
	public static void main(String[] args) {

		Shape circle = new CircleFactory().getShape(10); // On ne connait pas le type Réel qui est retourné (et tant
															// mieux : ce qui nous interresse c'est de disposer des
															// méthodes définies dans l'interface shape...)
		Shape square = new SquareFactory().getShape(10);

		System.out.println(circle.getSurface());
		System.out.println(square.getSurface());
	}
}
