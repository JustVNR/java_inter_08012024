package fr.dawan.creation.factory;

public class SquareFactory implements ShapeFactory{

	@Override
	public Shape getShape(double size) {
		
		return new Square(size);
	}
}