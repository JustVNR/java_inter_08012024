package fr.dawan.creation.factory;

public interface Shape {

	double getSurface();
}
