package fr.dawan.creation.factory;

public class CircleFactory implements ShapeFactory{

	@Override
	public Shape getShape(double size) {
		
		return new Circle(size);
	}
}
