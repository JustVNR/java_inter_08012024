package fr.dawan.creation.factory;

public class Circle implements Shape{

	private double radius;
	
	
	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	@Override
	public double getSurface() {
		
		return Math.PI * radius * radius;
	}

}
