package fr.dawan.creation.factory;

//public class ShapeFactory {
//
//	// PROBLEME : CETTE CLASSE N'EST PAS FERMEE A LA MODIFICATION
//	// SI J'AI BESOIN DE RAJOUTER UNE NOUVELLE FORME, JE DOIS MODIFIER LA CLASSE...
//	public Shape getShape(String shapeType, int dim) {
//		
//		if(shapeType == "Circle") {
//			return new Circle(dim);
//		}
//		else if(shapeType == "Square") {
//			return new Square(dim);
//		}
//		else if(shapeType == "Triangle") {
//			return new Square(dim);
//		}
//		return null;
//	}
//}

public interface ShapeFactory {

	Shape getShape(double size);
	
	// On pourrait créer une classe ShapeDescription qui utiliserait le pattern
	// Builder pour passer tous les paramètres nécessaires à la description de
	// n'importe quelle forme.
	// Shape getShape(ShapeDescription descr);
}
