package fr.dawan.creation.factory;

public class Square implements Shape{

	private double size;
	
	public Square(double size) {
		super();
		this.size = size;
	}

	@Override
	public double getSurface() {
		
		return size * size;
	}

}