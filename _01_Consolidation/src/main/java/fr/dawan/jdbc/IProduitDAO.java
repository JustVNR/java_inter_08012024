package fr.dawan.jdbc;

import java.util.List;

/*
 * Un DAO (Data Access Object) définit un ensemblde d eméthodes permettant d'interragir avec une BDD.
 * 
 * - CREATE
 * - READ
 * - UPDATE
 * - DELETE
 */
public interface IProduitDAO {

	List<Produit> getAll() throws Exception;

	Produit getById(int id) throws Exception;

	int insert(Produit p) throws Exception;

	void update(Produit p) throws Exception;

	void delete(int id) throws Exception;
}
