package fr.dawan.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProduitDAO implements IProduitDAO {
	private String properties;

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public ProduitDAO(String properties) {
		super();
		this.properties = properties;
	}

	@Override
	public List<Produit> getAll() throws Exception {

		List<Produit> lp = new ArrayList<Produit>();

		String sql = "SELECT * FROM produits";

		try (Connection cnx = JDBC.getConnection(properties);
				PreparedStatement ps = cnx.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();) {

			while (rs.next()) {

				Produit p = new Produit();

				p.setId(rs.getInt("id"));
				p.setDescription(rs.getString("description"));
				p.setPrix(rs.getDouble("prix"));

				lp.add(p);
			}
		}

		return lp;
	}

	@Override
	public Produit getById(int id) throws Exception {

		Produit p = null;

		String sql = "SELECT description, prix FROM produits WHERE id=?";

		try (Connection cnx = JDBC.getConnection(properties);
				PreparedStatement ps = cnx.prepareStatement(sql);) {

			ps.setInt(1,  id);
			
			try(ResultSet rs = ps.executeQuery();){
				
				if(rs.next()){
					p = new Produit(id, rs.getString("description"), rs.getDouble("prix"));
				}
			}
		}

		return p;
	}

	@Override
	public int insert(Produit p) throws Exception {
		
		String sql = "INSERT INTO produits(description, prix) VALUES(?,?)";
		
		try (Connection cnx = JDBC.getConnection(properties);
				PreparedStatement ps = cnx.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

			ps.setString(1,  p.getDescription());
			
			ps.setDouble(2,  p.getPrix());
			
			int affectedRows = ps.executeUpdate();
			
			if(affectedRows == 0) {
				throw new SQLException("La création du produit a échoué");
			}
			
			try(ResultSet generatedKeys = ps.getGeneratedKeys()){
				if(generatedKeys.next()) {
					p.setId(generatedKeys.getInt(1));
				}
				else {
					throw new SQLException("La création du produit a échoué");
				}
			}
		}
		
		return p.getId();
	}

	@Override
	public void update(Produit p) throws Exception {
		
		String sql = "UPDATE produits SET description=?,prix=? WHERE id=?";

		try (Connection cnx = JDBC.getConnection(properties);
				PreparedStatement ps = cnx.prepareStatement(sql);) {

			ps.setString(1, p.getDescription());
			ps.setDouble(2,  p.getPrix());
			ps.setInt(3, p.getId());
			
			ps.executeUpdate();
		}
	}

	@Override
	public void delete(int id) throws Exception {
		
		try (Connection cnx = JDBC.getConnection(properties);
				PreparedStatement ps = cnx.prepareStatement("DELETE FROM produits WHERE id=?");) {

			ps.setInt(1, id);
			
			ps.executeUpdate();
		}
	}
}
