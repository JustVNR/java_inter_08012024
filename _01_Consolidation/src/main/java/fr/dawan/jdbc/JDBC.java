package fr.dawan.jdbc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JDBC {

	public static Connection getConnection(String path) throws FileNotFoundException, IOException, SQLException{
		
		Connection cnx = null;
		
		try(InputStream input = new FileInputStream(path)){
			
			Properties props = new Properties();
			
			props.load(input);
			
			cnx = DriverManager.getConnection(props.getProperty("url"), props.getProperty("user"),
					props.getProperty("pwd"));
		}
		
		return cnx;
	}
}
