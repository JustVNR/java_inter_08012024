package fr.dawan.jdbc;

import java.util.List;

public class App {

	public static void main(String[] args) {

		try {
			IProduitDAO _dao = new ProduitDAO("conf.properties");
			
			List<Produit> produits = _dao.getAll();
			
			for (Produit p : produits) {
				_dao.delete(p.getId());
			}
			
			Produit produit = new Produit("Ordi", 550);
			
			int pId = _dao.insert(produit);
			
			produit = _dao.getById(pId);
			
			System.out.println(produit);
			
			produit.setPrix(600.5);
			
			_dao.update(produit);
			
			produit = _dao.getById(pId);
			
			System.out.println(produit);
			
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}

	}
}
