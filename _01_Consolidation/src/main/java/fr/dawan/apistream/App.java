package fr.dawan.apistream;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

// racourci pour indenter le code : CTRL + MAJ + F
// raccourci pour importer les packages manquants : CTRL + MAJ + O
// sysout + CTRL + ESPACE => System.out.println()
// pour commenter un bloc de code : CTRL + MAJ + :
// pour dupliquer la ligne en cours ou des lignes sélectionnées (en haut ou en bas) : CTRL + ALT + FLECHE HAUT / BAS
public class App {

	public static void main(String[] args) throws Exception {

		System.out.println("=========== STREAM API ===============\n");

		Stream<String> streamFromValues = Stream.of("a1", "a2", "a3");

		System.out.println("streamFromValues : " + streamFromValues.toList());

		// Stream from Array

		String[] array = { "a1", "a2", "a3" };

		Stream<String> streamFromArray = Arrays.stream(array);

		System.out.println("streamFromArray : " + streamFromArray.toList());

		// Stream from File
		File file = new File("myfile.tmp");

		PrintWriter pw = new PrintWriter(file);

		pw.println("a1");
		pw.println("a2");
		pw.println("a3");

		pw.close();

		try (Stream<String> streamFromFile = Files.lines(Paths.get(file.getAbsolutePath()))) {
			System.out.println("stremFromFile : " + streamFromFile.toList());
		}

		// Stream from Collection
		List<String> collection = List.of("a1", "a2", "a3");

		Stream<String> streamFromCollection = collection.stream();
		System.out.println("streamFromCollection : " + streamFromCollection.toList());

		System.out.println("\n============== FILTER ================\n");

		collection = List.of("a1", "a2", "a3", "a1", "a1");

		long nbA1 = collection.stream().filter("a1"::equals).count();

		System.out.println("Nombre de 'a1' : " + nbA1);

		List<String> chainesWitha1 = collection.stream().filter("a1"::contains).toList();

		System.out.println("chainesWitha1 : " + chainesWitha1);

		List<People> peoples = new ArrayList<People>();

		peoples.add(new People("William", 16, Sex.MAN));
		peoples.add(new People("John", 26, Sex.MAN));
		peoples.add(new People("Helene", 42, Sex.WOMAN));
		peoples.add(new People("Peter", 69, Sex.MAN));

		// liste des hommes entre 18 et 27 ans

		peoples.stream().filter(p -> p.getAge() >= 18 && p.getAge() <= 27 && p.getSex().equals(Sex.MAN)).toList()
				.forEach(System.out::println);

		// nombre d'invidus en age de travailler : entre 18 et 60 ans pour les hommes et
		// 55 ans pourles femmes

		long nbPeopleAtWork = peoples.stream().filter(p -> p.getAge() >= 18)
				.filter(p -> (p.getSex().equals(Sex.WOMAN) && p.getAge() <= 55)
						|| (p.getSex().equals(Sex.MAN) && p.getAge() <= 60))
				.count();

		System.out.println("nbPeopleAtWork = " + nbPeopleAtWork);

		System.out.println("\n==== .map() ======\n");

		// .map permet d'appliquer un traitement à chaque éléme,nt et retourne un
		// nouveau stream

		List<PeopleDTO> dtos = peoples.stream().map(p -> new PeopleDTO(p.getName(), p.getAge())).toList();

		dtos.forEach(System.out::println);

		// Pagination
		System.out.println("Pagination : ");
		peoples.stream().skip(1).limit(2).forEach(System.out::println);

		// Distinct

		collection = List.of("a1", "a2", "a3", "a1", "a1");

		collection.stream().distinct().forEach(System.out::println); // un seul a1...

		System.out.println("\n=============== .anyMatch, / allMatch / noneMatch =======================\n");

		boolean anyMatch = collection.stream().anyMatch(s -> s.equals("a1"));

		System.out.println("anyMatch = " + anyMatch);

		System.out.println("allMatch = " + collection.stream().allMatch(s -> s.contains("a")));

		System.out.println("noneMatch = " + collection.stream().noneMatch(s -> s.contains("b")));

		System.out.println("\n=============== .sorted =======================\n");

		collection.stream().sorted().forEach(System.out::println);

		System.out.println("------------");

		collection.stream().sorted().distinct().forEach(System.out::println);

		System.out.println("------------");

		collection.stream().sorted((s1, s2) -> -s1.compareTo(s2)).forEach(System.out::println); // tri par ordre
																								// décroissant

		System.out.println("\n=============== .max =======================\n");

		String max = collection.stream().max(String::compareTo).get();

		System.out.println("max = " + max);

		String min = collection.stream().min(String::compareTo).get();

		System.out.println("min = " + min);
		
		// compareTo n'accepte que des types complexes : il faut caster les 'int' en Integer
		
		People oldest = peoples.stream().max((p1, p2) -> ((Integer)p1.getAge()).compareTo((Integer)p2.getAge())).get();
		
		System.out.println("oldest = " + oldest);
		
		People yougest = peoples.stream().min((p1, p2) -> ((Integer)p1.getAge()).compareTo((Integer)p2.getAge())).get();
		
		System.out.println("yougest = " + yougest);
		
	}
}
