package fr.dawan.apitime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class App {

	public static void main(String[] args) {

		System.out.println("======= java.util.date (Avant java 8 ) =========\n");
		
		Date date = new Date();
		
		System.out.println("Aujourd'hui : " + date);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy hh:mm:ss");
		
		System.out.println(sdf.format(date));
		
		
		sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			
			Date date1 = sdf.parse("17/07/2015"); // parse : Créer un objet de type Date à partir d'une chaine de caractère
			Date date2 = sdf.parse("15/10/2001");
			
			if(date1.after(date2)) {
				System.out.println("date1 after date2");
			}
			else if(date1.before(date2)) {
				System.out.println("date1 before date2");
			}
			else if(date1.equals(date2)) {
				System.out.println("date1 equals date2");
			}
			
			if (date1.getMonth() < date2.getMonth()) {
				System.out.println("mois 1 < mois 2");
			}
						
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("======= java.time (Depuis java 8 ) =========\n");
		
		// java.time est (à ce jour) le package le plus récent pour gérer les dates, heures, instants, durées...
		// A utiliser de préférence sur les nouveaux projets
		System.out.println("LocalDate.now() = " + LocalDate.now());
		System.out.println("LocalDate.now().getMonth() = " + LocalDate.now().getMonth());
		System.out.println("LocalDate.now().getMonthValue() = " + LocalDate.now().getMonthValue());
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		System.out.println("LocalDate.now().format(dtf) = " + LocalDate.now().format(dtf));
		
		System.out.println("LocalDate.of(2004,  11, 5) = " + LocalDate.of(2004,  11, 5));
		
		System.out.println("LocalDateTime.now() = " + LocalDateTime.now());
		
		LocalDateTime in4days = LocalDateTime.now().plusDays(4);
		
		System.out.println("LocalDateTime.now().plusDays(4) = " + in4days);
		
		System.out.println("LocalDateTime.now().plusDays(4) = " + in4days.format(DateTimeFormatter.ofPattern("hh:mm:ss")));
		System.out.println("LocalDateTime.now().plusDays(4) = " + in4days.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
		
		System.out.println("======= FUSEAUX =========\n");
		
		// Fuseau horaire par défaut du système
		ZoneId fuseau = ZoneId.systemDefault();
		
		System.out.println("fuseau courant = " + fuseau);
		
		ZonedDateTime zoneDateTime = LocalDateTime.now().atZone(fuseau);
		
		// Heure actuelle dans le fuseu horaaire par défaut
		System.out.println("now here = " + zoneDateTime);
		
		System.out.println("now here = " + zoneDateTime.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
		
		// Obtention d'un autre fuseau horaire (per exmle UTC)
		
		fuseau = ZoneId.of("UTC");
		
		ZonedDateTime zoneDateTimeUTC = zoneDateTime.withZoneSameInstant(fuseau);
		
		System.out.println("now UTC = " + zoneDateTimeUTC.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
		
		System.out.println("======= INSTANT =========\n");
		
		System.out.println("Instant.EPOCH = " + Instant.EPOCH);
		System.out.println("Instant.now() = " + Instant.now());
		
		Instant parsedInstant = Instant.parse("2023-01-15T12:30:45Z");
		System.out.println("parsedInstant = " + parsedInstant);
		
		long milliSecondesEntreEpochEtParsedInstant = parsedInstant.toEpochMilli();
		
		System.out.println(milliSecondesEntreEpochEtParsedInstant);
		
		System.out.println("======= java.util.calendar (Depuis java 1.1) (OBSOLETE) =========\n");
		
		Calendar calendar = Calendar.getInstance(); // date et l'heure courante
		
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1; // ATTENTION les mois commencent à ZERO !!!!!
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		
		System.out.println("Date aujourd'hui : " + day + "-" + month + "-" + year);
		
		// Extraire la date
		
		date = calendar.getTime(); // getTime retoure un objet de type Date à partir d'un objet de type Calendar
		
		System.out.println("calendar.getTime() = " + date);
		
	}

}
