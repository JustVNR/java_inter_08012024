package fr.dawan.reflexion;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class App {

	public static void main(String[] args) throws Exception {

		// Reflexion / Introspection : mécanisme qui permet d'extraire des information
		// relative à un objet (type, champs, méthodes...) et de pouvoir appeler ses
		// méthodes dynamiquement

		Employe emp = new Employe("riri", "duck");

		// Obtenir le type à partir de l'objet

		Class<?> classEmp = emp.getClass();

		System.out.println("classEmp = " + classEmp);

		System.out.println("=============== METHODES DE LA CLASSE EMPLOYE =================\n");

		Method[] methods = classEmp.getMethods();
		
		for (Method method : methods) {
			System.out.println(method.getName());
		}
		
		System.out.println("=============== ATTRIBUTS DE LA CLASSE EMPLOYE =================\n");
		
		Field[] fields = classEmp.getDeclaredFields();
		
		for (Field field : fields) {
			System.out.println(field.getName());
		}
		
		System.out.println("=============== INVOQUER LA METHODE identite =================\n");
		
		Method methodIdentite = classEmp.getMethod("identite");
		
		methodIdentite.invoke(emp);
		
		System.out.println("=============== INSTANCIER DYNAMIQUEMENT LA CLASSE =================\n");
		
		Employe instance = (Employe)classEmp.getDeclaredConstructor().newInstance(); // Appel au constructeur ans paraamètre
		
		methodIdentite.invoke(instance);
		
		Constructor<?> constructor = classEmp.getConstructor(String.class, String.class);
		
		Employe instance2 = (Employe)constructor.newInstance("fifi", "duck");
		
		methodIdentite.invoke(instance2);
	}

}
