package fr.dawan.interfacesfonctionnelles;

public class MethodsReference {

	public int methodInstance(int a, int b) {

		return a + b;
	}
	
	public static int methodStatic(int a, int b) {

		return a + b;
	}
}
