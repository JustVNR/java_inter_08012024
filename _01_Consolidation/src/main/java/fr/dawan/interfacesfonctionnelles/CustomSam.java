package fr.dawan.interfacesfonctionnelles;

@FunctionalInterface
public interface CustomSam {
	int add(int a, int b);
	// int multiply(int a, int b); INTERDIT : une seule méthode ABSTRAITE autorisée dans une interface fonctionnelle
}
