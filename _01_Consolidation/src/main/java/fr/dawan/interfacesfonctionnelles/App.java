package fr.dawan.interfacesfonctionnelles;

import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class App {

	/*
	 * Un interface fonctionnelle (Single Abstrat Method (SAM)) est une interface
	 * qui possède exactement une méthode abstraite.
	 * 
	 * Introduites en java 8
	 * 
	 * Les interfaces fonctionnelles permettent notamment de créer des expressions
	 * lambdas
	 */
	public static void main(String[] args) {

		int[] nombres = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		int sommeCarresNombresPairs = Arrays.stream(nombres).filter(n -> n % 2 == 0) // filtrer les nombre impairs
				.map(n -> n * n).sum();

		System.out.println("sommeCarresNombresPairs = " + sommeCarresNombresPairs);

		System.out.println("\n================ IntUnaryOperator ================\n");

		IntUnaryOperator multiplyByTwo = x -> x * 2;

		Arrays.stream(nombres).map(multiplyByTwo).forEach(System.out::println);

		System.out.println("\n================  BUILT IN JAVA SAM ================\n");

		System.out.println("\n================ PREDICATE ================\n");
		// un Predicate retourne un booléen

		Predicate<String> frenchTelValidator = numero -> {

			String regex = "(\\+33|0)[1-9](\\d{2}){4}";

			return Pattern.matches(regex, numero);
		};

		System.out.println(frenchTelValidator.test("0612365478"));
		System.out.println(frenchTelValidator.test("123450612365478"));

		Predicate<String> USTelValidator = numero -> {

			String regex = "^(\\d{3}-\\d{3}-\\d{4}|\\(\\d{3}\\) \\d{3}-\\d{4})$";

			return Pattern.matches(regex, numero);
		};

		System.out.println(frenchTelValidator.or(USTelValidator).test("0612365478"));

		// BiPredicate est doublement générique
		BiPredicate<String, String> patternValidator = (numero, pattern) -> {
			return Pattern.matches(pattern, numero);
		};

		System.out.println(patternValidator.test("0612365478", "(\\+33|0)[1-9](\\d{2}){4}"));

		System.out.println("\n================ FUNCTION ================\n");
		// Function : reçoit une valeur et en retourne une autre

		Function<Integer, Integer> incrementByOne = a -> a + 1;

		System.out.println(incrementByOne.apply(5)); // 6

		Function<Integer, Integer> myltiplyBy10 = a -> a * 10;

		Function<Integer, Integer> incrementByOneAndThenMultipliBy10 = incrementByOne.andThen(myltiplyBy10);

		System.out.println(incrementByOneAndThenMultipliBy10.apply(5)); // 60

		Function<Integer, Integer> incrementByOneAfterMultipliBy10 = incrementByOne.compose(myltiplyBy10);

		System.out.println(incrementByOneAfterMultipliBy10.apply(5)); // 51

		BiFunction<Integer, Integer, Integer> incrementAndThenMultiply = (nToIncrement, multiple) -> (nToIncrement + 1)
				* multiple;

		System.out.println(incrementAndThenMultiply.apply(10, 5)); // 55

		System.out.println("\n================ CONSUMER ================\n");
		// Consumer retourne void (utilisé dans stream().forEach()

		Consumer<String> samPrintString = System.out::println;

		samPrintString.accept("Chaine affichée via un Consumer");

		BiConsumer<String, Integer> samPrintStringIf = (str, length) -> {

			if (str.length() < length) {
				System.out.println(str);
			}
		};

		samPrintStringIf.accept("Message court", 15);
		samPrintStringIf.accept("Message trop long", 15); // Ne sera pas affiché...

		System.out.println("\n================ CUSTOM SAM ================\n");

		// Comment implémenter une interface fonctionnelle?

		// option 1 : créer une classe qui implémente l'interface

		System.out.println(new CustomSamImpl().add(2, 3));

		// option 2 : utiliser une expression lambda

		CustomSam customSam = (a, b) -> a + b;

		System.out.println(customSam.add(2, 3));

		// option 3 : utiliser une classe anonyme

		customSam = new CustomSam() {

			@Override
			public int add(int a, int b) {

				return a + b;
			}
		};

		System.out.println(customSam.add(2, 3));

		// option 4 : Méthode référence : Faire référence à une méthode qui respecte la
		// signature de la méthode définie dans l'interface fonctionnelle
		
		customSam = MethodsReference::methodStatic; // ref vers une méthode static
		
		System.out.println(customSam.add(2, 3));
		
		customSam = new MethodsReference()::methodInstance; // ref vers une méthode d'instance
		
		System.out.println(customSam.add(2, 3));
	}
}
