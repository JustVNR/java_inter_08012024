package fr.dawan.genericite.serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class BIN{

	public static void encodeToFile(Object obj, String path) throws FileNotFoundException, IOException {
		
		try(ObjectOutput oos = new ObjectOutputStream(new FileOutputStream(path));){
			oos.writeObject(obj);
		}
	}
	
	public void encodeToFileInstance(Object obj, String path) throws FileNotFoundException, IOException {
		
		try(ObjectOutput oos = new ObjectOutputStream(new FileOutputStream(path));){
			oos.writeObject(obj);
		}
	}
	
	public static Object decodeFromFile(String path) throws FileNotFoundException, IOException, ClassNotFoundException {
		
		Object result = null;
		
		try(ObjectInput ois = new ObjectInputStream(new FileInputStream(path))){
			result = ois.readObject();
		}
		
		return result;
	}
}
