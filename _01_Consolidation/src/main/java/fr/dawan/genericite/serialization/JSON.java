package fr.dawan.genericite.serialization;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

public class JSON {

	public static void encodeToFile(Object obj, String path) throws IOException {

		try (FileWriter fw = new FileWriter(path);) {
			new Gson().toJson(obj, fw);
		}
	}

	public static <T> List<T> decodeFromFile(String path, Type clazz) throws FileNotFoundException, IOException {

		T[] tab = null;

		try (FileReader fr = new FileReader(path);) {
			
			tab = new Gson().fromJson(fr, clazz);
		}

		return Arrays.asList(tab);
	}
}
