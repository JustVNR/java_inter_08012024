package fr.dawan.genericite.serialization;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class CSV {

	public static void encodeToFile(List<?> obj, String path) throws IOException, IllegalArgumentException, IllegalAccessException {

		// 1;PC;1200
		
		try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path));){

			for(Object p : obj) {
				
				Field[] fields = p.getClass().getDeclaredFields();
				
				StringBuilder sb = new StringBuilder();
				
				for(Field f : fields) {
					
					// PB : les champs sont privés...
					// Il faut les rendre accessibles
					f.setAccessible(true);
					
					sb.append(f.get(p).toString()).append(";");
				}
				sb.append("\n");
				
				bos.write(sb.toString().getBytes());
			}
		}
	}

	public static <T> List<T> decodeFromFile(String path, Class<T> clazz) throws FileNotFoundException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {

		List<T> lst = new ArrayList<T>();
		
		try(BufferedReader br = new BufferedReader(new FileReader(path))){
			
			String ligne = null;
			
			while((ligne = br.readLine()) != null) {
				// 1;pc;1500
				
				// Découpage de la ligne en cours
				String[] tab = ligne.split(";");
				
				T obj = clazz.getDeclaredConstructor().newInstance();
				
				Field[] fields =clazz.getDeclaredFields();
				
				int i = 0;
				
				for(Field field : fields) {
					
					field.setAccessible(true);
					
					Class<?> type = field.getType();
					
					if(type.equals(String.class)) {
						field.set(obj, tab[i]);
					}
					else if(type.equals(double.class)) {
						field.set(obj, Double.parseDouble(tab[i]));
					}
					else if(type.equals(int.class)) {
						field.set(obj, Integer.parseInt(tab[i]));
					}
					i++;
				}
				
				lst.add(obj);
			}
		}
		
		return lst;
	}
}
