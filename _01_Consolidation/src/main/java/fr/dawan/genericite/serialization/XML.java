package fr.dawan.genericite.serialization;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class XML {

	public static void encodeToFile(Object obj, String path) throws FileNotFoundException, IOException {

		try (XMLEncoder encoder = new XMLEncoder(new FileOutputStream(path));) {
			encoder.writeObject(obj);
		}
	}

	public static Object decodeFromFile(String path) throws FileNotFoundException, IOException, ClassNotFoundException {

//		Object result = null;
//
//		try (XMLDecoder ois = new XMLDecoder(new FileInputStream(path))) {
//			result = ois.readObject();
//		}
//
//		return result;
		
		try (XMLDecoder ois = new XMLDecoder(new FileInputStream(path))) {
			return  ois.readObject();
		}
	}
}
