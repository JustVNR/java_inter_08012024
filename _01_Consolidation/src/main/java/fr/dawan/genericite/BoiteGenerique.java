package fr.dawan.genericite;

public class BoiteGenerique<T /* extends ClassMere on peut rajouter des contraintes sur le type génrique */> {

	private T contenu;

	public T getContenu() {
		return contenu;
	}

	public void setContenu(T contenu) {
		this.contenu = contenu;
	}

	public BoiteGenerique(T contenu) {
		super();
		this.contenu = contenu;
	}

	// Exemple de méthode générique
	// le ou les types génériques peut / peuvent être différents du : des type(s)
	// générique de la classe mère (si elle est elle-même générique)
	public <U> void afficherContenu(U prefix) {
		System.out.println(prefix.toString() + contenu);
	}
}
