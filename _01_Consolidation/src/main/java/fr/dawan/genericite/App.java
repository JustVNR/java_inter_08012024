package fr.dawan.genericite;

import java.util.ArrayList;
import java.util.List;

import fr.dawan.genericite.serialization.BIN;
import fr.dawan.genericite.serialization.CSV;
import fr.dawan.genericite.serialization.JSON;
import fr.dawan.genericite.serialization.XML;

public class App {

	public static void main(String[] args) {

		// Exemple d'utilisation avec une BoiteGenerique de type String
		BoiteGenerique<String> boiteString = new BoiteGenerique<String>("Hello World !");

		System.out.println("Contenu de la boite = " + boiteString.getContenu());

		// Exemple d'utilisation avec une BoiteGenerique de type String
		BoiteGenerique<Integer> boiteInteger = new BoiteGenerique<Integer>(42);

		System.out.println("Contenu de la boite = " + boiteInteger.getContenu());
		
		// Utilisation d'une méthode générique
		
		boiteString.afficherContenu("Etiquette : ");
		boiteString.afficherContenu(1);
		
		
		System.out.println("\n=================== SERIALISATION ======================");
		
		List<Product> products = new ArrayList<Product>();
		
		System.out.println("nombre de produits = " + Product.getnInstances());
		
		products.add(new Product(1, "PC", 999.99));
		products.add(new Product(2, "ECRAN", 250.99));
		products.add(new Product(3, "CLAVIER", 99.99));
		
		System.out.println("nombre de produits = " + Product.getnInstances());
		
		try {
			
			System.out.println("\n=================== BINAIRE ======================\n");
			
			BIN.encodeToFile(products, "produits.bin");
			
			((List<?>)BIN.decodeFromFile("produits.bin")).stream().forEach(System.out::println);
			
			System.out.println("\n=================== XML ======================\n");
			
			XML.encodeToFile(products, "produits.xml");
			
			((List<?>)XML.decodeFromFile("produits.xml")).stream().forEach(System.out::println);
			
			System.out.println("\n=================== JSON ======================\n");
			
			JSON.encodeToFile(products, "produits.json");
			
			JSON.decodeFromFile("produits.json", Product[].class).stream().forEach(System.out::println);
			
			System.out.println("\n=================== CSV ======================\n");
			
			CSV.encodeToFile(products, "produits2.csv");
			
			CSV.decodeFromFile("produits2.csv", Product.class).stream().forEach(System.out::println);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
