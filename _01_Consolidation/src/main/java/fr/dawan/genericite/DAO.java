package fr.dawan.genericite;

import java.util.List;

// DAO : Data Access Objet... 
// le terme "repository" est également très souvent utilisé...
public interface DAO<T> {

	List<T> getAll();
	void insert(T obj);
	// update, delete...
}
