package fr.dawan.threads;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class MyRunnable implements Runnable{

	private String src;
	
	private String dest;
	
	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public MyRunnable(String src, String dest) {
		super();
		this.src = src;
		this.dest = dest;
	}

	@Override
	public void run() {

		try {
			Files.copy(Path.of(src), Path.of(dest), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
