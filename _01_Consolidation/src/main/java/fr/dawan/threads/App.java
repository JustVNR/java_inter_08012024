package fr.dawan.threads;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class App {

	static int total;

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		// Thread : unité physique d'exécution d'une tâche
		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {

				System.out.println("Thread 1...");
			}
		});

		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {

				System.out.println("Thread 2...");
			}
		});

		t1.start();
		t2.start();

		t1.join(); // met en attente le thread principal jusqu'à ce que t1 soit terminé
		System.out.println("t1 a términé sa tâche");
		t2.join();

		System.out.println("currentThread().getName() = " + Thread.currentThread().getName());

		t1 = new Thread(App::addMillion);
		t2 = new Thread(App::addMillion);

		System.out.println("total = " + total);
		t1.start();
		t2.start();

		System.out.println("total = " + total);
		t1.join();
		System.out.println("total = " + total);
		t2.join();
		System.out.println("total = " + total);

		new Thread(new MyRunnable("a.txt", "a2.txt")).start();
		new Thread(new MyRunnable("b.txt", "b2.txt")).start();

		// Depuis Java 8, l'interface 'ExecutorService' permet de créer un pool de
		// threads
		ExecutorService executor = Executors.newFixedThreadPool(2);

		executor.execute(new MyRunnable("c.txt", "c2.txt"));
		executor.execute(new MyRunnable("d.txt", "d2.txt"));

		// Exécuter une tâche dans un tjread et récupérer le résultat de la ta^che
		// asynchrone
		int x = 10;
		int y = 15;

		int resultat = 0;

		Future<Integer> futureInt = executor.submit(new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {

				Thread.sleep(3000);
				return x + y;
			}
		});

		resultat = futureInt.get(); // Waits if necessary for the computation to complete, and thenretrieves its
									// result.

		System.out.println("Resultat = " + resultat);

		/*
		 * CompletableFuture (Java 8) étend les fonctionnalités de 'Future' en offrant
		 * de méthodes permettant de composer des opérations, de gérer les erreurs, etc...
		 */

		CompletableFuture<String> futurStr = CompletableFuture.supplyAsync(() -> "Hello", executor);

		futurStr.thenApplyAsync(r -> r + " World", executor).thenAcceptAsync(System.out::println).join();

		executor.shutdown(); // L'exécutor n'accepte plus de taches...
	}

	// le mot-clé synchronized est utilisé pour définir des sections de code
	// qui ne peuvent être exécuté que dans un seul thread à la fois.
	// Une méthode 'synchronized' permet d'éviter les problèmes liés à l'exécution
	// simultanée de plusieurs threads
	// utilisant des ressources communes (ici la variable 'total')
	public synchronized static void addMillion() {
		for (int i = 0; i < 1000000; i++) {
			total++;
		}
	}

}
