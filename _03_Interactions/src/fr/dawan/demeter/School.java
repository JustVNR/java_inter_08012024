package fr.dawan.demeter;

import java.util.List;

public class School {

	/*
	 * La loi de Demeter, également connue sous le principe
	 * "ne parez qu'à vos amis proches" est un principe de conception qui encourage
	 * à réduire les dépendances entre classes.
	 * 
	 * Elle stipule que chaque classe devrait avoir une connaissance limitée des
	 * classes dont elle ne dépend pas directement.
	 * 
	 * Exemple la classe School ne dépend pas directement de la classe 'Student'.
	 * 
	 * Et qu'elle ne devrait interragir qu'avec ses "amis" proches, idéalement
	 * qu'avec les éléements de la classe elle-même.
	 * 
	 * A savoir dans le présent exemple, la classe School ne devrait interragir
	 * qu'avec la classe 'StudentClass' et pas avec la classe 'Student'
	 */
	private List<StudentClass> classes;

	public int middleAgeNotDemeter() {

		double somme = 0;

		for (StudentClass sc : classes) {

			for (Student s : sc.getStudents()) {

				somme += s.getAge();
			}
		}

		double count = 0;

		for (StudentClass sc : classes) {

			count += sc.getStudents().size();
		}

		return (int) (somme / count);
	}
	
	public int middleAgeDemeter() {

		double somme = 0;

		double count = 0;

		for (StudentClass sc : classes) {

			somme += sc.middleAge() * sc.getStudents().size();
			
			count += sc.getStudents().size();
		}

		return (int) (somme / count);
	}
}
