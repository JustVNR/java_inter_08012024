package fr.dawan.demeter;

import java.util.ArrayList;
import java.util.List;

public class StudentClass {

	private List<Student> students;

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public StudentClass() {
		super();
		this.students = new ArrayList<Student>();
	}
	
	public StudentClass(List<Student> students) {
		super();
		this.students = students;
	}
	
	public int middleAge() {
		
		double somme = 0;
		
		for (Student student : students) {
			
			somme += student.getAge();
		}
		
		return (int) (somme /students.size());
	}
}
