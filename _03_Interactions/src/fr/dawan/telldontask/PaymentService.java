package fr.dawan.telldontask;

public class PaymentService {

	/*
	 * Tell dont Ask : ne posez pas de questions.
	 * Dites à vos objets ce qu'ils doivent faire, ne leur posez pas de question sur leur état.
	 */
	public void payAsk(Account acc, double amount) {
		
		if(acc.getBalance() < amount) { // ASK : ne respecte pas le principe Tell Dont Ask
			throw new IllegalArgumentException("Solde insuffisant");
		}

		acc.setBalance(acc.getBalance() - amount);
	}
	
	// respecte "Tell dont ask"
	public void payTell(Account acc, double amount) {
		
		try {
			acc.withdraw(amount);
		}
		catch(Exception e ){
			e.printStackTrace();
		}
	}
}
