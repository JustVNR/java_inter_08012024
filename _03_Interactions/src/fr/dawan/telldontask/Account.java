package fr.dawan.telldontask;

public class Account {

	private int id;

	private double balance;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		
		// vérifier que balanace est positif 
		
		if(balance < 0) {
			throw new IllegalArgumentException("Une valeur de sold doit être positive");
		}
		this.balance = balance;
	}

	public Account(int id, double balance) {
		super();
		this.id = id;
		this.balance = balance;
	}

	public void withdraw(double amount) throws IllegalArgumentException{

		if(balance < amount) {
			throw new IllegalArgumentException("Solde insuffisant");
		}
		
		balance -= amount;
		
		// setBalance(getBalance() - amount);
	}
}
