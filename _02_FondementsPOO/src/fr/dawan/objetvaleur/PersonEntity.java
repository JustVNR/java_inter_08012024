package fr.dawan.objetvaleur;

public class PersonEntity {

	private PersonId id;

	private FirstName firsName;

	private LastName lastName;

	private EmailAdress email;

	public PersonEntity(PersonId id, FirstName firsName, LastName lastName, EmailAdress email) {
		super();
		this.id = id;
		this.firsName = firsName;
		this.lastName = lastName;
		this.email = email;
	}

	@Override
	public String toString() {
		return "PersonEntity [id=" + id.getValue() + ", firsName=" + firsName.getValue() + ", lastName="
				+ lastName.getValue() + ", email=" + email.getValue() + "]";
	}
}

class PersonId {

	private long value;

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public PersonId(long value) {
		super();
		this.value = value;
	}
}

class FirstName {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public FirstName(String value) {
		super();
		this.value = value;
	}
}

class LastName {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public LastName(String value) {
		super();
		this.value = value;
	}
}

class EmailAdress {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public EmailAdress(String value) {
		super();
		this.value = value;
	}
}