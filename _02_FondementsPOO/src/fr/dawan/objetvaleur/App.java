package fr.dawan.objetvaleur;

public class App {

	public static void main(String[] args) {


		Person person = new Person(1, "riri", "duck", "riri@gmail.com");
		
		// erreur : firstName et lastName sont inversés...
		person = new Person(1, "duck", "riri", "riri@gmail.com"); // compile malgré l'erreur d'ordonnacement 
		
		System.out.println(person);
		
		PersonEntity personEntity = new PersonEntity(
				new PersonId(1L),
				new FirstName("riri"),
				new LastName("duck"),
				new EmailAdress("riri@gmail.com")
				);
		
		// Ne compile pas grace à l'utilisation "d'objets valeur"
//		personEntity = new PersonEntity(
//				new PersonId(1L),
//				new LastName("duck"),
//				new FirstName("riri"),
//				new EmailAdress("riri@gmail.com")
//				);

		System.out.println(personEntity);
	}

}
