package fr.dawan.objetvaleur;

public class Person {

	private long id;
	
	private String firsName;
	
	private String lastName;
	
	private String email;

	// rajouter Getters et Setters...
	
	public Person(long id, String firsName, String lastName, String email) {
		super();
		this.id = id;
		this.firsName = firsName;
		this.lastName = lastName;
		this.email = email;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", firsName=" + firsName + ", lastName=" + lastName + ", email=" + email + "]";
	}
}
