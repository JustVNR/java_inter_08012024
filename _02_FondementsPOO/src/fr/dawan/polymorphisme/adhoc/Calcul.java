package fr.dawan.polymorphisme.adhoc;

public class Calcul {

	/*
	 * Le polymorphisme adhoc représente la possiblité pour une même classe de définir plusieurs méthodes (surchage)
	 * 
	 * - de même nom
	 * 
	 * - mais possédant des signatures différentes
	 */
	public static int addition(int int1, int int2) {
		return int1 + int2;
	}
	
	public static float addition(float f1, float f2) {
		return f1 + f2;
	}
	
	public static double addition(double d1, double d2) {
		return d1 + d2;
	}
}
