package fr.dawan.polymorphisme.soustypage;

public interface IPliable {

	/*
	 * Le polymorphisme par sous typage repose sur l'héritage et l'implémentation d'interface
	 */
	void plier();
	void deplier();
}
