package fr.dawan.polymorphisme.soustypage;

public class Chaise implements IPliable{

	@Override
	public void plier() {
		System.out.println("plier chaise");	
	}

	@Override
	public void deplier() {
		System.out.println("déplier chaise");	
	}
}
