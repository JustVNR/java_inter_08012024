package fr.dawan.polymorphisme.soustypage;

public class Table implements IPliable{

	@Override
	public void plier() {
		System.out.println("plier table");
	}

	@Override
	public void deplier() {
		System.out.println("déplier table");
	}
}
