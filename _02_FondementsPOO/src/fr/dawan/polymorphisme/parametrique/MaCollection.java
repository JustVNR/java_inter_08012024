package fr.dawan.polymorphisme.parametrique;

import java.util.ArrayList;
import java.util.List;

public class MaCollection<T> {

	/*
	 * Le polymorphisme paramétirque permet de définir des types génériques
	 * 
	 * qui pourront au besoin être instanciés avec différents types.
	 */
	List<T> items = new ArrayList<T>();
	
	public void add(T item) {
		items.add(item);
	}
	
}
