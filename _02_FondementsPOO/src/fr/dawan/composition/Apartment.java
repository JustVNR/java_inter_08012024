package fr.dawan.composition;

public class Apartment {

	/*
	 * La composition est une agrégation forte : les composants de l'objet ne peuvent exister sans lui.
	 * La cuisine ne peut exister sans l'appartement.
	 */
	private Kitchen kitchen;

	// Getters Setters...
	
	public Apartment() {
		super();
		this.kitchen = new Kitchen(); // la cuisine est créée en même temps que l'appartement
	}
	
	// ...
}
