package fr.dawan.encapsulation;

public class App {

	/*
	 * L'encapsulation est un mécanisme consistant à rassembler les attributs et
	 * méthodes d'un objet au sein d'une structure cachant son implémentation.
	 * 
	 * Le but étant d'empécher l'accès aux données par un autre moyen que les services proposés par l'objet.
	 * 
	 * L'encapsulation permet donc notamment de garantir l'intégrité des données contenues dans l'objet.
	 * 
	 * En java le principe d'encapsulation s'illustre notamment par l'utilisation de méthodes permettant : 
	 * 
	 * - d'accéder / lire (Getters)
	 * - de modifier / écrire (Setters)
	 * 
	 * les attributs d'une classe.
	 */
	public static void main(String[] args) {

		Rectangle rec = new Rectangle();

		rec.largeur = -10;

		rec.longueur = 100;

		System.out.println("rec.surface() = " + rec.surface());

		RectangleEncapsule recEncapsule = new RectangleEncapsule();

		// recEncapsule.largeur = -10; // larger n'est pas directement accessible...

		// rec.longueur = 100;

		// recEncapsule.setLargeur(-10);
		
		recEncapsule.setLargeur(10);
		
		recEncapsule.setLongueur(100);

		System.out.println("recEncapsule = " + recEncapsule);
		
		RectangleEncapsule recEncapsule2 = new RectangleEncapsule(10, 100);

		System.out.println("recEncapsule2.surface() = " + recEncapsule2.surface());

	}

}
