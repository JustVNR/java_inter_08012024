package fr.dawan.encapsulation;

public class RectangleEncapsule {

	private int largeur;
	private int longueur;

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) throws IllegalArgumentException {

		if (largeur < 0) {
			throw new IllegalArgumentException("Une largeur doit être positive");
		}
		
		this.largeur = largeur;
	}

	public int getLongueur() {
		
		// on pourrait aussi rajouter une logique pour filtrer l'accès en lecture...
		return longueur;
	}

	public void setLongueur(int longueur) {
		
		if (longueur < 0) {
			throw new IllegalArgumentException("Une longueur doit être positive");
		}
		this.longueur = longueur;
	}

	public RectangleEncapsule() {
		super();
		
	}
	
	public RectangleEncapsule(int largeur, int longueur) {
		super();
//		this.largeur = largeur;
//		this.longueur = longueur;
		
		this.setLargeur(largeur);
		this.setLongueur(longueur);
	}

	@Override
	public String toString() {
		return "RectangleEncapsule [largeur=" + largeur + ", longueur=" + longueur + "]";
	}
	
	public int surface() {
		return largeur * longueur;
	}

}
