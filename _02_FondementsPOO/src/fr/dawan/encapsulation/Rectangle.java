package fr.dawan.encapsulation;

public class Rectangle {

	public int longueur; // le monde extérieur à directement accès !!

	public int largeur;  // les champs largeur et longeur ne sont pas encapsulés !!

	public Rectangle() {

	}
	
	public Rectangle(int largeur, int longueur) {
		this.largeur = largeur;
		this.longueur = longueur;
	}

	public int surface() {
		return largeur * longueur;
	}
}
