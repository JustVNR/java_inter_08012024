package fr.dawan.agregation;

import java.util.ArrayList;
import java.util.List;

public class Department {

	private String name;
	
	private List<Employe> employes;

	// Getters et Setters...
	
	public Department(String name) {
		super();
		this.name = name;
		this.employes = new ArrayList<Employe>();
	}
	
	public void addEmploye(Employe e) {
		employes.add(e);
	}

	@Override
	public String toString() {
		
		String strEmployes = "";
		
		for (Employe employe : employes) {
			strEmployes += "\t- " + employe.toString() + "\n";
		}
		return "Department " + name + "\n" + strEmployes;
	}
	
}
