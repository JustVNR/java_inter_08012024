package fr.dawan.agregation;

public class Employe {

	private int id;
	
	private String name;

	// Getters et Setters...
	
	public Employe(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Employe [id=" + id + ", name=" + name + "]";
	}
}
