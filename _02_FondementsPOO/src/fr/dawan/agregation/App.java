package fr.dawan.agregation;

public class App {

	/*
	 * L'agrégation est un concept de POO permettant de modéliser la relation entre
	 * un objet et ce qui le compose. Il s'agit d'une forme de relation de type
	 * "a / possède un" où un objet contient d'autres objets (ex : un département
	 * contient des employes) en tant que comosants.
	 * 
	 * Dans le cas de l'agrégation, les comosant peuvent exister par eux mêmes. Ex :
	 * les employés peuvent exister sans le département.
	 * 
	 * La composition est une agrégation forte : les composants de l'objet ne peuvent exister sans lui.
	 * La cuisine ne peut exister sans l'appartement.
	 */
	public static void main(String[] args) {

		Employe e1 = new Employe(1, "riri");
		Employe e2 = new Employe(2, "fifi");

		Department department = new Department("R&D");

		department.addEmploye(e1);
		department.addEmploye(e2);

		System.out.println(department);
	}

}
