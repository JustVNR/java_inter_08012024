package fr.dawan.heritage;

public class Animal {

	private String nom;

	public Animal(String nom) {
		super();
		this.nom = nom;
	}
	
	public void manger() {
		System.out.println(nom + " mange.");
	}
	
	public void faireDuBruit() {
		System.out.println("L'animal fait du bruit.");
	}
}
