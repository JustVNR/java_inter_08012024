package fr.dawan.heritage;

public class App {
	
	/*
	 * L'héritage permet de créer des classes spécialisées à partir d'une / de classe(s)
	 * regroupant des traits et des comportements communs.
	 * 
	 * En Java (et dans la plupart des langages) il n'est possible d'hériter sque d'une classe (en ascendance directe)
	 * Et ce pour évitre le "problème du diamand" :
	 * https://fr.wikipedia.org/wiki/Problème_du_diamant
	 */
	public static void main(String[] args) {

		Animal animal = new Animal("animal");
		animal.manger();
		animal.faireDuBruit();
		
		System.out.println();
		
		Aligator ali = new Aligator("ali");
		ali.manger();
		ali.faireDuBruit();
		ali.remuerLaQueue();
		
		System.out.println();
		
		Sinensis sinensis = new Sinensis("sinensis");
		sinensis.manger(); // appel de la méthode de la classe Animal
		sinensis.faireDuBruit(); // appel de la méthode de la classe Aligator
		sinensis.remuerLaQueue(); // appel de la méthode de la classe Aligator
		sinensis.nager(); // appel de la méthode spécialée dans la classe Sinensis
		
	}
}
