package fr.dawan.heritage;

public class Aligator extends Animal{

	public Aligator(String nom) {
		super(nom); // appel le constructeur de la superclasse
	}

	@Override
	public void faireDuBruit() {
		System.out.println("L'aligator gronde");
	}
	
	// On peut rajouter des méthodes et des attributs aux classes filles pourles spécialiser...
	public void remuerLaQueue() {
		System.out.println("L'aligator remue la queue");
	}
	
	
}
