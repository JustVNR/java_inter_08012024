package fr.dawan.heritage;

/*
 * Il existe deux principales espèces d'alligators :
 * 
 * l'alligator américain (Alligator mississippiensis) et l'alligator de Chine
 * (Alligator sinensis).
 */

public class Sinensis extends Aligator {

	public Sinensis(String nom) {
		super(nom);
		// TODO Auto-generated constructor stub
	}

	// Nouvelle méthodes spécifique à la classe Sinensis
	public void nager() {
		System.out.println("Le Sinensis nage en chine");
	}
	
	
}
