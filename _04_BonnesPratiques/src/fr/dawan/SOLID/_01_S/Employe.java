package fr.dawan.SOLID._01_S;

public class Employe {

	/*
	 * Principe de responsabilité unique (Single responsibility principle (SRP))
	 * 
	 * Le SRP stipule qu'une classe ne devrait avoir qu'une seule resposabilité, et
	 * 
	 * donc qu'une seule raison de changer.
	 */
	private int id;

	private String name;

	private String address;

	// Ne respecte le SRP : ce n'est pas du ressort de la classe Employe de gérer sa
	// peristence en base de données
	// A la place on va créer une couche d'accès aux données
	public void save() {

	}

	// Ne respecte pas le SRP non plus : A la place on va créer un service de
	// Ressource Humaines qui va gérer cet aspect de la gestion des employés
	public boolean isPromotionDueThisYear() {
		return false;
	}
}
