package fr.dawan.SOLID._04_I.NOK;

import java.util.List;

public interface IUserService {

	/*
	 * Le principe de ségrégation des interfaces (Interfzce Segregation Principle ISP)
	 * 
	 * stipule qu'aucun client : utilisateur ne devrait dépendre de méthodes qu'il n'utilise pas.
	 * 
	 * L'idée est donc de diviser les interfaces volumineuses en plus petites et plus spécifique,
	 * 
	 * de sorte que les utilisateurs n'aient globalement accès qu'à des méthodes interessantes pour eux.
	 * 
	 * Le but étant de maintenir un système à couplage le plus faible possible.
	 */
	List<User> getAll();
	
	User getById(int id);
	
	void delete(int id);
	
	void insert(User u);
	
	void update(User u);
	
	// Problème : l'interface gère à la fois la persitance des User et l'aspect Commercial.
	
	String getDateInscription();
	
	String getSubscriptionType();
	
	// On va donc splitter l'interface en 2.
	
}
