package fr.dawan.SOLID._04_I.OK;

import java.util.List;

public interface PersistenceService {

	List<User> getAll();

	User getById(int id);

	void delete(int id);

	void insert(User u);

	void update(User u);
}
