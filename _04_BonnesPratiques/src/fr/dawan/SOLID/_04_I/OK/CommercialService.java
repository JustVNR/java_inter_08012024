package fr.dawan.SOLID._04_I.OK;

public interface CommercialService {

	String getDateInscription();

	String getSubscriptionType();
}
