package fr.dawan.SOLID._03_L.NOK;

public class App {

	/*
	 * Liskov substitution principle : une instance de type T doit pouvoir être
	 * remplacée par une instance de type G, tel que G sous-type de T, sans que cela
	 * ne modifie la cohérence du programme.
	 * 
	 * En d'autres termes, dans le cadre d'une relation d'héritage, un objet enfant
	 * doit pouvoir remplacer un objet parent sans effets de bords.
	 */
	public static void main(String[] args) {

		Bird bird = new Eagle();
		
		bird.fly();
		
		bird = new Penguin();
		
		bird.fly(); // un pingouin ne peut pas voler => principe de substitution de Liskov non respecté
	}
}
