package fr.dawan.SOLID._03_L.NOK;

public class Penguin implements Bird{

	@Override
	public void fly() {
		throw new UnsupportedOperationException("A penguin cannot fly...");
	}
}
