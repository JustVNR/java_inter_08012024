package fr.dawan.SOLID._03_L.OK;

public class Penguin implements SwimmingBird {

	@Override
	public void swimm() {
		System.out.println("Le pingouin nage...");
	}
}
