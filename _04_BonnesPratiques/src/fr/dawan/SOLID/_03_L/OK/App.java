package fr.dawan.SOLID._03_L.OK;

public class App {

	public static void main(String[] args) {

		FlyingBird eagle = new Eagle();
		
		eagle.fly();
		
		SwimmingBird penguin = new Penguin();
		
		penguin.swimm();
	}
}
