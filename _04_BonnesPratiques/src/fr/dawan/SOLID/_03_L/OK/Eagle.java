package fr.dawan.SOLID._03_L.OK;

public class Eagle implements FlyingBird{

	@Override
	public void fly() {
		System.out.println("L'aigle vole");
	}
}
