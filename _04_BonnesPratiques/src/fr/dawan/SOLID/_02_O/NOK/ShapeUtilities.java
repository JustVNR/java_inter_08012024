package fr.dawan.SOLID._02_O.NOK;

public class ShapeUtilities {

	/*
	 * Principe Open /Closed
	 * 
	 * Une classe doit être ouverte à l'extension et fermée à la modification
	 * 
	 * De nouvelles fonctionnalités doivent pouvoir être ajoutées à l'appliquation
	 * sans modifier l'existant.
	 */
	public double airRectangle(double largeur, double hauteur) {
		return largeur * hauteur;
	}

	public double airDisque(double rayon) {
		return Math.PI * rayon * rayon;
	}

	// Problème : la classe est ouverte à la modification : si j'ai besoin de
	// calculer la surface d'un triangle, je dois la modifier...
}
