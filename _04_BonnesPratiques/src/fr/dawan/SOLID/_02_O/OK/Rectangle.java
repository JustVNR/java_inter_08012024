package fr.dawan.SOLID._02_O.OK;

public class Rectangle implements Shape {

	private double largeur;
	
	private double longueur;
	

	public double getLargeur() {
		return largeur;
	}

	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}

	public double getLongueur() {
		return longueur;
	}

	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}

	@Override
	public double calculSurface() {
		
		return largeur * longueur;
	}
}
