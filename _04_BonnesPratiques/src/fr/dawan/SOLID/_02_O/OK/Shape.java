package fr.dawan.SOLID._02_O.OK;

public interface Shape {

	/*
	 * Le principe Open / Closed est respecté. Il n'y a pas besoin de modifier
	 * l'interface Shape et les classes qui l'implémentent pour ajouter de nouvelles
	 * fonctionnalités à l'application.
	 * 
	 * Exemple : si on a besoin de calculer la surface d'un triangle, il suffit de
	 * rajouter une classe Triangle qui implémente l'interface Shape.
	 */
	double calculSurface();
}
