package fr.dawan.SOLID._02_O.OK;

public class Disque implements Shape{

	private double rayon;
	
	public double getRayon() {
		return rayon;
	}

	public void setRayon(double rayon) {
		this.rayon = rayon;
	}

	@Override
	public double calculSurface() {
		
		return Math.PI * rayon * rayon;
	}
}
