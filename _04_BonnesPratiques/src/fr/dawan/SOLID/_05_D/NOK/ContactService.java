package fr.dawan.SOLID._05_D.NOK;

public class ContactService {

	/*
	 * Le principe d'inversion de dépendances (Depdency Inversion Principle (DIP))
	 * 
	 * vise à réduire le couplage entre les différents composants d'un système.
	 * 
	 * Il vise à dépendre des abstractions (i.e. des interfaces) plutôt que de leurs
	 * implémentations.
	 */
	public Contact getById(int id) {

		ContactDAO repo = new ContactDAO(); // Problème : la couche service dépend de l'implémentation de la couche DAO

		Contact c = repo.getById();

		return c;
	}
}
