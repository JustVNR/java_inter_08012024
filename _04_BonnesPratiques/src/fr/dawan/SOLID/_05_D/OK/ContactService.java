package fr.dawan.SOLID._05_D.OK;

public class ContactService {

	// La classe ContactService dépend de l'abstraction IContactDAO et non pas d'une
	// de ses implémentations.
	private IContactDAO _dao;

	// option 1 : injection de dépendance via constructeur
	// Avantage : Un objet de type ContactService est créée dans un état stable
	// Inconvénient : impossible de changer de dépendance entre chaque appel de méthode
	public ContactService(IContactDAO dao) {
		super();
		this._dao = dao;
	}

	public Contact getById(int id) {

		Contact c = _dao.getById(id);

		return c;
	}
	
	// option 2 : injection de dépendance via les paramètres de la méthode
	// Avantage : possiblité de modifier la dépendance en utilisant le même objet ContactService
	// Inconvénient : dépendanecà fournir à chaque appel de la méthode
	public ContactService() {
		super();
	}
	
	public Contact getById(int id, IContactDAO dao) {

		Contact c = dao.getById(id);

		return c;
	}

	
	// option 3 : injection via le setter
	// Avantage : dépendance est modifiable à tout instant
	// Inconvénient : il faut s'assurer que la dépendance à bien été injectée (à n'utiliser que si necessaire)
	public void set_dao(IContactDAO dao) {
		this._dao = dao;
	}
	
}
