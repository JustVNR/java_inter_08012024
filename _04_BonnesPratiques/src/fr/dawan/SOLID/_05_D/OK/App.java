package fr.dawan.SOLID._05_D.OK;

public class App {

	/*
	 * Il y a plusieurs manières d'injecter une dépendance
	 * 
	 * - option 1 : via le constructeur
	 * 
	 * - option 2 : via les paramètres des méthodes
	 * 
	 * - option 3 : via le setter
	 */
	public static void main(String[] args) {

		// option 1
		ContactService service = new ContactService(new ContactDAOSqlServer());
		
		service = new ContactService(new ContactDAOMySQL());
		
		service.getById(1);
		

	}

}
