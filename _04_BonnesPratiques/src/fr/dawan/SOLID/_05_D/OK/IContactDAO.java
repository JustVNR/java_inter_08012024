package fr.dawan.SOLID._05_D.OK;

public interface IContactDAO {

	Contact getById(int id);
}
