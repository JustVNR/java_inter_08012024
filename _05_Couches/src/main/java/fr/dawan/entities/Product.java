package fr.dawan.entities;

public class Product {

	private int id;
	
	private String description;
	
	private int prix;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public Product(int id, String description, int prix) {
		super();
		this.id = id;
		this.description = description;
		this.prix = prix;
	}
}
