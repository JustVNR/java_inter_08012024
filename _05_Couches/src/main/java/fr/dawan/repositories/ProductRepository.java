package fr.dawan.repositories;

import java.util.List;

import fr.dawan.entities.Product;

public interface ProductRepository {

	List<Product> getAll() throws Exception;

	Product getById(int id) throws Exception;

	int insert(Product p) throws Exception;

	void update(Product p) throws Exception;

	void delete(int id) throws Exception;
	
}
