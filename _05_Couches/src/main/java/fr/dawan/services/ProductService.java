package fr.dawan.services;

import java.util.List;

import fr.dawan.dtos.ProductDTO;

public interface ProductService {

	List<ProductDTO> getAll() throws Exception;

	ProductDTO getById(int id) throws Exception;

	int insert(ProductDTO p) throws Exception;

	void update(ProductDTO p) throws Exception;

	void delete(int id) throws Exception;
}
