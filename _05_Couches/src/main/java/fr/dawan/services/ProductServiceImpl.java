package fr.dawan.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import fr.dawan.Tools.DtoTools;
import fr.dawan.dtos.ProductDTO;
import fr.dawan.repositories.ProductRepository;

public class ProductServiceImpl implements ProductService {

	private ProductRepository _repo;

	public ProductServiceImpl(ProductRepository _repo) {
		super();
		this._repo = _repo;
	}

	@Override
	public List<ProductDTO> getAll() throws Exception {

		List<ProductDTO> dtos = new ArrayList<ProductDTO>();
	
		_repo.getAll().stream().forEach(p -> {
			dtos.add(DtoTools.convert(p, ProductDTO.class));
		});

		return dtos;
	}

	@Override
	public ProductDTO getById(int id) throws Exception {
		
		ProductDTO dto = DtoTools.convert(_repo.getById(id), ProductDTO.class);
		
		return dto;
	}

	@Override
	public int insert(ProductDTO p) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void update(ProductDTO p) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) throws Exception {
		// TODO Auto-generated method stub

	}

}
