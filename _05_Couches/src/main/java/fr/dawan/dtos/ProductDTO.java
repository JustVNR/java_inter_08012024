package fr.dawan.dtos;


/*
 * Il faut faire autant de Dtos que nécessaire.
 * 
 * Ne pas faire un seul grand Dto qui sert pour toutes les requêtes.
 * 
 * ex : 
 * 
 * - InsertProductDTO
 * - UpdateProductDTO
 * - IndexProductDTO
 * - DetailsProductDTO
 * ....
 */
public class ProductDTO {

	private String description;
	
	private int prix;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public ProductDTO(String description, int prix) {
		super();
		this.description = description;
		this.prix = prix;
	}
	
}
