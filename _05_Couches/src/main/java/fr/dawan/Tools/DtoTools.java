package fr.dawan.Tools;

import org.modelmapper.ModelMapper;

public class DtoTools {

	private static ModelMapper mapper = new ModelMapper();
	
	
	private DtoTools() {
		
	}
	
	public static <TSource, TDestination> TDestination convert(TSource obj, Class<TDestination> clazz) {
		
		// Au besoin : Ajouter des règles de mapping personalisées...
		
		try {
			return mapper.map(obj,  clazz);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
