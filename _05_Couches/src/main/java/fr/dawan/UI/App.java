package fr.dawan.UI;

import fr.dawan.dtos.ProductDTO;
import fr.dawan.repositories.ProductRepositoryImpl;
import fr.dawan.services.ProductService;
import fr.dawan.services.ProductServiceImpl;

public class App {

	/*
	 * L'architecture en couche est un modèle d'organisation d'un système logiciel.
	 * 
	 * Elle sépare les différentes fonctionnalités en groupes distincts appelés couches.
	 * 
	 * Le but étant de favoriser la modularité, la maaintenabilité, et la réutilisabibilté du code.
	 * 
	 * Pricipes fondamentaux de l'Architecture en couche : 
	 * 
	 * - 1 : Séparation des Préoccupations
	 * 
	 * - 2 : Abstraction
	 * 
	 * Exemples courants de couches :
	 * 
	 * 1 Présentation (Interface utilisateur)
	 * 
	 * 2 Couche métier
	 * 
	 * 3 Couche d'accès aux données.
	 * 
	 */
	public static void main(String[] args) {

		ProductDTO p = new ProductDTO("PC", 1000);
		
		ProductService _service = new ProductServiceImpl(new ProductRepositoryImpl());
		
		try {
			_service.insert(p);
			System.out.println("Inserted...");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
